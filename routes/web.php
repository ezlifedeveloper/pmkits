<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\RenunganController;
use App\Http\Livewire\AbsensiComponent;
use App\Http\Livewire\Admin\EventManagement;
use App\Http\Livewire\Admin\KepengurusanManagement;
use App\Http\Livewire\Admin\KonselingManagement;
use App\Http\Livewire\Admin\NewsManagement;
use App\Http\Livewire\Admin\PrayerRequestManagement;
use App\Http\Livewire\Admin\ProfilManagement;
use App\Http\Livewire\Admin\RenunganManagement;
use App\Http\Livewire\Admin\RiwayatOrganisasiManagement;
use App\Http\Livewire\Admin\UserManagement;
use App\Http\Livewire\EventDetailComponent;
use App\Http\Livewire\EventFinishedComponent;
use App\Http\Livewire\EventListComponent;
use App\Http\Livewire\GalleryComponent;
use App\Http\Livewire\LoginComponent;
use App\Http\Livewire\NewsListComponent;
use App\Http\Livewire\ParticipantComponent;
use App\Http\Livewire\PrayerRequestComponent;
use App\Http\Livewire\ProfileComponent;
use App\Http\Livewire\RenunganListComponent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index'])->name('home');

Route::prefix('backoffice')->middleware('loggedin:administrator')->group(function () {
    Route::view('dashboard', 'admin.dashboard')->name('dashboard');
    Route::get('user', UserManagement::class)->name('adminuser');
    Route::get('event', EventManagement::class)->name('adminevent');
    Route::get('news', NewsManagement::class)->name('adminnews');
    Route::get('profil', ProfilManagement::class)->name('adminprofil');
    Route::get('renungan', RenunganManagement::class)->name('adminrenungan');
    Route::get('prayerrequest', PrayerRequestManagement::class)->name('adminprayerrequest');
    Route::view('event/{id}', 'admin.eventdetail')->name('admineventdetail');
    Route::get('kepengurusan', KepengurusanManagement::class)->name('adminkepengurusan');
    Route::get('riwayatorganisasi', RiwayatOrganisasiManagement::class)->name('adminriwayatorganisasi');
    Route::get('konseling', KonselingManagement::class)->name('adminkonseling');

    Route::middleware('access:Bendahara')->group(function(){
        Route::get('kepengurusan', KepengurusanManagement::class)->name('adminkepengurusan');

    });
    Route::middleware('access:Dope')->group(function(){
        Route::get('prayerrequest', PrayerRequestManagement::class)->name('adminprayerrequest');
        Route::get('konseling', KonselingManagement::class)->name('adminkonseling');
    });
});


Route::middleware('loggedin:Alumni,Dosen,Mahasiswa')->group(function () {
    Route::get('profile', ProfileComponent::class)->name('profile.index');

});

Route::get('event', EventListComponent::class)->name('event.index');
Route::get('event/{id}/{mobile?}/{username?}/{admin?}/{iduser?}', EventDetailComponent::class)->name('event.detail');
Route::get('finished/{id}/{mobile?}', EventFinishedComponent::class)->name('event.finished');
Route::get('news', NewsListComponent::class)->name('news.index');
Route::get('news/{id}', [NewsController::class, 'show'])->name('news.detail');
Route::get('renungan', RenunganListComponent::class)->name('renungan.index');
Route::get('prayer', PrayerRequestComponent::class)->name('prayer.index');
Route::get('renungan/{id}', [RenunganController::class, 'show'])->name('renungan.detail');

Route::get('attend/{event}/{username?}/{admin?}/{mobile?}/{iduser?}', AbsensiComponent::class)->name('absensi');

Route::middleware('notloggedin')->get('login', LoginComponent::class)->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

<?php

use App\Http\Controllers\API\AuthApi;
use App\Http\Controllers\API\EventApi;
use App\Http\Controllers\API\EventPresenceApi;
use App\Http\Controllers\API\KepengurusanManagementApi;
use App\Http\Controllers\API\KonselingApi;
use App\Http\Controllers\API\NewsApi;
use App\Http\Controllers\API\NotifApi;
use App\Http\Controllers\API\PrayerRequestApi;
use App\Http\Controllers\API\RenunganApi;
use App\Http\Controllers\API\UserApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1')->group(function () {
    Route::get('/kepengurusan', [KepengurusanManagementApi::class, 'index']);

    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthApi::class, 'login']);
        Route::post('logout', [AuthApi::class, 'logout']);
    });

    Route::prefix('users')->middleware('auth:api')->group(function () {
        Route::get('/', [UserApi::class, 'index']);
        Route::get('/{id}', [UserApi::class, 'show']);
        Route::put('/{id}', [UserApi::class, 'update']);
        Route::post('/register', [UserApi::class, 'store']);
    });

    Route::get('/notif/{id}', [NotifApi::class, 'show']);

    Route::get('/event', [EventApi::class, 'index']);
    Route::get('/event/{id}', [EventApi::class, 'show']);

    Route::post('/event_presence', [EventPresenceApi::class, 'store']);

    Route::get('/news', [NewsApi::class, 'index']);

    Route::get('/renungan', [RenunganApi::class, 'index']);

    Route::get('/prayer_request', [PrayerRequestApi::class, 'index']);
    Route::post('/prayer_request', [PrayerRequestApi::class, 'store']);

    Route::get('/konseling', [KonselingApi::class, 'index']);
    Route::post('/konseling', [KonselingApi::class, 'store']);
});

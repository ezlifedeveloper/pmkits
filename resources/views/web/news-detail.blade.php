@extends('web.app')

@section('content')
    <!-- Event-->
    <section class="bg-gray" id="blog">
        <div class="container">
            <div class="row grid-pad">
                <div class="col-sm-5 col-xs-12">
                    <img class="text-center" width="100%" src="{{$data->cover}}" alt="{{$data->title}}">
                </div>
                <div class="col-sm-5 col-xs-12">
                    <h3>
                        {{$data->title}}<br/>
                        <small>{{$data->created_at->diffForHumans()}}</small>
                    </h3>

                    <p class="text-description">{{ $data->description }}</p>
                    @if($data->url) <a style="color:blue" href="{{$data->url}}" target="_blank">[link]</a> @endif

                    @if(isset($data->file) && count($data->file)>0)
                    <h5>Lampiran</h5>
                    @foreach ($data->file as $f)
                    <p style="margin-bottom: 0px">
                        {{$loop->index+1}}. {{$f->name}}
                        <a style="color:blue" href="{{$f->file}}" target="_blank">[download]</a>
                    </p>
                    @endforeach
                    @endif
                </div>
            </div>

            @livewire('nav-footer-component', ['menu' => $menu, 'state' => 'detail', 'id' => $data->id])
        </div>

    </section>

@endsection

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}">
        <meta name="description" content="PMK ITS adalah organisasi mahasiswa yang bergerak dalam bidang kerohanian di ITS dan berada dibawah naungan TPKK ITS">
        <meta name="author" content="EZ-Life Developer">
        <meta property="og:description" content="PMK ITS adalah organisasi mahasiswa yang bergerak dalam bidang kerohanian di ITS dan berada dibawah naungan TPKK ITS" />
        <meta property="og:url" content="https://sites.its.ac.id/tpkk" />
        <meta property="og:image" content="{{ asset('PMK.png') }}" />
        <meta property="og:title" content="{{ env("APP_NAME") }}" />
        <meta property="og:type" content="portfolio" />
        <meta property="og:locale:alternate" content="in_ID" />

        <title>{{$title.' | '.env('APP_NAME')}}</title>

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico')}}" />

        @include('web.layout.css')
        @livewireStyles
    </head>

    <body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        @if(!Request::has('mobile'))
            @if($title!='Login' && !($menu=='Event' && isset($data->isFeatured))) @include('web.layout.nav') @endif
        @endif

        @include('web.layout.header')

        @if(!($menu=='Event' && isset($data->isFeatured))) @yield('content') @endif

        @if(!Request::has('mobile'))
        @if($menu!='Login' && !($menu=='Event' && isset($data->isFeatured))) @include('web.layout.footer') @endif
        @endif

        <a class="topbtn page-scroll" href="#page-top"></a>
        @include('web.layout.js')
        @livewireScripts
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
        </script>
        <x-livewire-alert::scripts />
    </body>
</html>

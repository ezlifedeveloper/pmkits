@extends('web.app')

@section('content')
    <!-- About-->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>About us</h2>
                    <p style="margin-bottom: 10px">{{ $data['profil']->detail }}</p>
                </div>
                <div class="col-lg-5 col-lg-offset-1" style="display: inline-block; height: 100%;">
                    <h3>&nbsp;</h3>
                    <div class="row text-center" style="padding:0">
                        <img class="col-lg-3 col-xs-6" style="object-fit: contain; height: 50px; margin-bottom: 40px" src="{{asset('TPKK.png')}}"/>
                        <img class="col-lg-3 col-xs-6" style="object-fit: contain; height: 50px; margin-bottom: 40px" src="{{asset('PMK.png')}}"/>
                        <img class="col-lg-3 col-xs-6" style="object-fit: contain; height: 50px; margin-bottom: 40px" src="{{asset('PKMBK.png')}}"/>
                        <img class="col-lg-3 col-xs-6" style="object-fit: contain; height: 50px; margin-bottom: 40px" src="{{asset('NAPAS.png')}}"/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Event-->
    <section class="bg-gray" id="blog">
        <div class="container text-center">
            <h2 class="no-pad">Latest Events<a class="fa fa-plus-circle fa-fw gray" href="{{route('event.index')}}" title="See All"></a></h2>
            <div class="row grid-pad">
                @foreach ($data['event'] as $d)
                <div class="col-sm-4">
                    <a href="{{route('event.detail', $d->slug)}}">
                        <img class="img-responsive center" height="300px" src="{{$d->cover}}" alt="">
                        <h4>
                            {{$d->name}}<br/>
                            <small>{{$d->start->diffForHumans()}}</small>
                        </h4>
                    </a>
                    <p>{{$d->excerpt}}</p>
                    {{-- <a class="btn btn-gray" href="{{route('event.detail', $d->slug)}}">Read more</a> --}}
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- Facts section-->
    <section class="facts bg-img-custom-small" style='background-image: url("{{asset('bgheader.jpeg')}}");'>
        <div class="overlay"></div>
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <i class="ion-ios-stopwatch-outline icon-big"></i>
                    <span class="numscroller" data-min="0" data-max="78" data-delay="5" data-increment="1">0</span>
                    Event
                </div>
                <div class="col-sm-3 col-xs-6">
                    <i class="ion-android-clipboard icon-big"></i>
                    <span class="numscroller" data-min="0" data-max="29" data-delay="5" data-increment="1">0</span>
                    Dosen
                </div>
                <div class="col-sm-3 col-xs-6">
                    <i class="ion-ios-body-outline icon-big"></i>
                    <span class="numscroller" data-min="0" data-max="2785" data-delay="5" data-increment="3">0</span>
                    Mahasiswa
                </div>
                <div class="col-sm-3 col-xs-6">
                    <i class="ion-ios-gear-outline icon-big fa-spin"></i>
                    <span class="numscroller" data-min="0" data-max="12" data-delay="5" data-increment="1">0</span>
                    Alumni
                </div>
            </div>
        </div>
    </section>

    <!-- Menu-->
    <section>
        <div class="container-fluid pricing">
            <h2 class="text-center">Renungan</h2>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="row">
                        @foreach ($data['renungan'] as $d)
                        <a href="{{route('renungan.detail', $d->slug)}}">
                            <div class="col-md-4 col-sm-6">
                                <div class="panel panel-default" style="background-color: #a62a08">
                                    <img class="img-responsive" height="255px" src="{{$d->cover}}" />

                                    <div style="padding: 10px; height: 180px">
                                        <div class="row">
                                            <div class="col-xs-6 text-left">
                                                <p style="color: #ffffff; margin-bottom:5px">{{$d->date->format('d M Y')}}</p>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <p style="color: #ffffff; margin-bottom:5px"><b>{{$d->verse}}</b></p>
                                            </div>
                                        </div>
                                        <h4 style="color: #ffffff">{{$d->title}}</h4>
                                        <p style="color: #ffffff">{{$d->excerpt}}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Gallery-->
    <section class="bg-gray" id="gallery">
        <div class="container-fluid text-center">
        <h2>Our Gallery</h2>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">
                    @livewire('gallery-component')
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection

@section('js')
<script src="{{ asset('web/js/jquery.countdown.min.js') }}"></script>
@endsection

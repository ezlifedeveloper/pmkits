@extends('web.app')

@section('content')
    <!-- Event-->
    <section class="bg-gray" id="blog">
        <div class="container">
            <div class="row grid-pad">
                <div class="col-sm-4 col-xs-12">
                    <img class="text-center" width="100%" src="{{$data->cover}}" alt="{{$data->title}}">
                </div>
                <div class="col-sm-8 col-xs-12">
                    <h3>
                        <div class="row" style="padding-bottom: 0">
                            <small class="col-lg-6">{{$data->verse}}</small>
                            <small class="col-lg-6 text-right">{{$data->date->format('d M Y')}}</small>
                        </div>
                        {{$data->title}}
                    </h3>

                    <p style="text-align: justify; white-space:pre-wrap; word-wrap:break-word">{!! $data->description !!}</p>
                </div>
            </div>
            @livewire('nav-footer-component', ['menu' => $menu, 'state' => 'detail', 'id' => $data->date])
        </div>
    </section>
@endsection

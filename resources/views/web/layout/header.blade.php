@if($menu=='Login')
<header class="intro intro-fullscreen" data-background="{{asset('ITS.jpeg')}}">
    <div class="overlay"></div>
    @yield('login')
</header>
@elseif($menu=='Home')
<!-- Intro Slider-->
<header class="intro carousel carousel-big slide carousel-fade" id="Carousel-intro" data-ride="carousel">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li class="active" data-target="#Carousel-intro" data-slide-to="0"></li>
        @if(count($data['news'])>0)<li data-target="#Carousel-intro" data-slide-to="1"></li>@endif
        @if(count($data['news'])>1)<li data-target="#Carousel-intro" data-slide-to="2"></li>@endif
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style='background-image: url("{{asset('file/profilepmk/profile-homecover.png')}}");'>
                <div class="intro-body">
                <div class="overlay"></div>
                <h1>
                    <span class="big">
                        <div class="span">Welcome to</div>
                        <div class="span blue">PMK ITS
                    </span>
                </h1>
                @livewire('social-media-component')
                </div>
            </div>
        </div>

        @foreach ($data['news'] as $d)
        <div class="item">
            <div class="fill" style='background-image: url("{{$d->cover}}");'>
                <div class="intro-body">
                    <div class="overlay"></div>
                    <h1>
                        <span>
                            <small><div class="span blue">Latest News</div></small>
                            <div class="span">{{$d->title}}</div>
                        </span>
                    </h1>
                    <ul class="list-inline">
                        <li><a class="btn btn-white wow fadeInLeft" href="{{route('news.detail', $d->slug)}}">Selengkapnya</a></li>
                        <li><a class="btn btn-border wow fadeInRight" href="{{route('news.index')}}">Berita Lainnya</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- Controls-->
    @if(count($data['news'])>0)
    <a class="left carousel-control" href="#Carousel-intro" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#Carousel-intro" data-slide="next">
        <span class="icon-next"></span>
    </a>
    @endif
</header>
@elseif($menu=='Event' && isset($data->isFeatured))
<header class="intro" data-background="{{$data->hdcover ?? $data->cover}}">
    <div class="@if(!$data->report) intro-fullscreen @endif" style="height: 100%; width:100%; background: rgba(0, 0, 0, 0.5);">
        <div class="magic2" id="magic2">
            <canvas id="magic2-canvas"></canvas>
        </div>
        <div class="overlay"></div>
        <div class="container" style="max-width:100%;">
            <div class="row">
                @if($data->report) <div class="col-sm-10 col-sm-offset-1">
                @else <div class="col-sm-8 col-sm-offset-2">
                @endif
                    <img class="logolanding" src="{{asset('PMK.png')}}" alt="PMK ITS" style="max-width:80%">
                    @yield('content')
                    @if(!$mobile)<a class="btn btn-dark" href="{{ URL::previous() }}">KEMBALI</a>@endif
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 10px">
            <div class="container">
                <div class="footer text-center">
                    @livewire('social-media-component')
                </div>
            </div>
        </div>
    </div>
</header>
@elseif(!isset($mobile) && !Request::has('mobile'))
<div class="small-header bg-img-custom" style='background: url("{{asset('file/profilepmk/profile-banner.png')}}");'>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2></h2>
            </div>
            <div class="col-md-6 text-right">
                <h4 class="breadcrumb">
                    <a href="{{route('home')}}">Home</a> / <a href="{{route(strtolower($menu).'.index')}}">{{$menu}}</a>
                </h4>
            </div>
        </div>
    </div>
</div>
@endif

<!-- footer - alignment center-->
<section class="section-small bg-dark footer">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-4">
                @livewire('social-media-component')
            </div>
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
                <h5 class="no-pad">Developed by <a href="https://ezlife.id">Ez Life</a>
                </h5>
            </div>
        </div>
    </div>
</section>

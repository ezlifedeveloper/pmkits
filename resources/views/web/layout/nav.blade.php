<!-- Navigation-->
<nav class="navbar navbar-custom navbar-center navbar-fixed-top top-nav-collapse">
    <div class="container">
        <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-main-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand page-scroll" href="#page-top">
            <img class="logo" src="{{asset('PMK.png')}}" alt="Logo">
            <img class="logodark" src="{{asset('PMK.png')}}" alt="Logo"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling-->
        <div class="collapse navbar-collapse navbar-main-collapse">
        <ul class="nav navbar-nav">
            <li @if($menu=='Home') class="active" @endif><a href="{{route('home')}}">Home</a></li>
            <li @if($menu=='Event') class="active" @endif><a href="{{route('event.index')}}">Event</a></li>
            <li @if($menu=='News') class="active" @endif><a href="{{route('news.index')}}">News</a></li>
            <li @if($menu=='Renungan') class="active" @endif><a href="{{route('renungan.index')}}">Renungan</a></li>
        </ul>
        <ul class="nav navbar-nav pull-right">
            <li>
                <a href="{{route('prayer.index')}}">Prayer</a>
            </li>
            @if(Auth::check() && (Auth::user()->role == 1 || Auth::user()->role == 2))
                @if (Auth::user()->role == 1)
                <li>
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                @elseif (Auth::user()->role == 2 && Auth::user()->access != null)
                <li>
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                @endif
            @endif
            @if(Auth::check()&& Auth::user()->role != 1)
            <li>
                <a href="{{route('profile.index')}}">Profile</a>
            </li>
            @endif
            <li>
                @if(!Auth::check()) <a href="{{route('login')}}">Login</a>
                @else <a href="{{route('logout')}}">Logout</a>
                @endif
            </li>
        </ul>
        </div>
    </div>
</nav>

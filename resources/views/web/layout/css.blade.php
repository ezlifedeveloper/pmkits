<!-- Bootstrap Core CSS-->
<link href="{{ asset('web/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Custom CSS-->
<link href="{{ asset('web/css/main.css') }}" rel="stylesheet">

<style>
    .text-description {
        text-align  : justify;
        white-space : pre-wrap;
        word-wrap   : break-word;
    }

    .icon-button {
        width   :30px;
        height  :30px;
        margin  : 1px;
    }
</style>

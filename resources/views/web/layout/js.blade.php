<!-- jQuery-->
<script src="{{ asset('web/js/jquery-3.3.1.min.js') }}"></script>
<!-- Bootstrap Core JavaScript-->
<script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
<!-- Plugin JavaScript-->
<script src="{{ asset('web/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('web/js/device.min.js') }}"></script>
<script src="{{ asset('web/js/form.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.shuffle.min.js') }}"></script>

@if((isset($menu) && $menu!='Event') || (isset($data->isFeatured) && !$mobile))
<script src="{{ asset('web/js/jquery.parallax.min.js') }}"></script>
<script src="{{ asset('web/js/magic2.min.js') }}"></script>
@endif
<script src="{{ asset('web/js/jquery.circle-progress.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.swipebox.min.js') }}"></script>
<script src="{{ asset('web/js/wow.min.js') }}"></script>
<script src="{{ asset('web/js/text-rotator.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.smartmenus.js') }}"></script>
<!-- Custom Theme JavaScript-->
<script src="{{ asset('web/js/main.js') }}"></script>

@yield('js')

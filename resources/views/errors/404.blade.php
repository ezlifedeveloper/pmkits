<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}">
        <meta name="description" content="PMK ITS adalah organisasi mahasiswa yang bergerak dalam bidang kerohanian di ITS dan berada dibawah naungan TPKK ITS">
        <meta name="author" content="EZ-Life Developer">
        <meta property="og:description" content="PMK ITS adalah organisasi mahasiswa yang bergerak dalam bidang kerohanian di ITS dan berada dibawah naungan TPKK ITS" />
        <meta property="og:url" content="https://sites.its.ac.id/tpkk" />
        <meta property="og:image" content="{{ asset('PMK.png') }}" />
        <meta property="og:title" content="{{ env("APP_NAME") }}" />
        <meta property="og:type" content="portfolio" />
        <meta property="og:locale:alternate" content="in_ID" />

        <title>{{'Not Found | '.env('APP_NAME')}}</title>

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico')}}" />

        @include('web.layout.css')
    </head>
    <body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Preloader (Optional)-->
        <div id="preloader">
        <div id="status"></div>
        </div>
        <!-- Header-->
        <header class="intro intro-fullscreen" data-background="{{asset('404.jpg')}}">
        <div class="overlay"></div>
        <div class="intro-body">
            <h1 class="big light">404</h1>
            <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                <h3>Looks like you got lost</h3>
                @if(!Request::has('mobile'))<a class="btn btn-dark" href="{{ route('home') }}">Home</a>@endif
                </div>
            </div>
            </div>
        </div>
        </header>
        @include('web.layout.js')

    </body>
</html>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Laravel Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/style.css") }}" media="screen" />
        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/print.css") }}" media="print" />
        <script src="{{ asset("vendor/scribe/js/all.js") }}"></script>

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/highlight-darcula.css") }}" media="" />
        <script src="{{ asset("vendor/scribe/js/highlight.pack.js") }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body class="" data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">
<a href="#" id="nav-button">
      <span>
        NAV
            <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="-image" class=""/>
      </span>
</a>
<div class="tocify-wrapper">
                <div class="lang-selector">
                            <a href="#" data-language-name="bash">bash</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI (Swagger) spec</a></li>
                            <li><a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: November 15 2021</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://localhost:8000";
</script>
<script src="{{ asset("vendor/scribe/js/tryitout-2.7.10.js") }}"></script>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://localhost:8000</code></pre><h1>Authenticating requests</h1>
<p>This API is not authenticated.</p><h1>Auth Management</h1>
<p>APIs for User Auth</p>
<h2>Login Default</h2>
<p>Login Default, on success you'll get a 200 OK response.
Return 401 when user is not found / authenticated.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"prasetyon","password":"passw0rd","token":"aBc4e6Gh"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "prasetyon",
    "password": "passw0rd",
    "token": "aBc4e6Gh"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-auth-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-auth-login"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-auth-login"></code></pre>
</div>
<form id="form-POSTapi-v1-auth-login" data-method="POST" data-path="api/v1/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-auth-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-auth-login" onclick="tryItOut('POSTapi-v1-auth-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-auth-login" onclick="cancelTryOut('POSTapi-v1-auth-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-auth-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/auth/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>username</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="username" data-endpoint="POSTapi-v1-auth-login" data-component="body" required  hidden>
<br>
User's username
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password" data-endpoint="POSTapi-v1-auth-login" data-component="body" required  hidden>
<br>
User's password.
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-v1-auth-login" data-component="body" required  hidden>
<br>
User's device token.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User list.
</p>
<h2>Logout</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Logout, on success you'll get a 200 OK response.
Return 401 when user is not found / authenticated.
Return 422 when parameter is not filled.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/auth/logout?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":"999","token":"aBc4e6Gh"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/auth/logout"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": "999",
    "token": "aBc4e6Gh"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-auth-logout" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-auth-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-auth-logout"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-auth-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-auth-logout"></code></pre>
</div>
<form id="form-POSTapi-v1-auth-logout" data-method="POST" data-path="api/v1/auth/logout" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-auth-logout', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-auth-logout" onclick="tryItOut('POSTapi-v1-auth-logout');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-auth-logout" onclick="cancelTryOut('POSTapi-v1-auth-logout');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-auth-logout" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/auth/logout</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-auth-logout" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="POSTapi-v1-auth-logout" data-component="body" required  hidden>
<br>
User's id
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-v1-auth-logout" data-component="body" required  hidden>
<br>
User's device token.
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User list.
</p><h1>Event Management</h1>
<p>APIs for managing Event Data</p>
<h2>Get Event List</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Event List, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/event?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=dolorum&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/event"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "dolorum",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get Event Data",
    "data": []
}</code></pre>
<div id="execution-results-GETapi-v1-event" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-event"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-event"></code></pre>
</div>
<div id="execution-error-GETapi-v1-event" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-event"></code></pre>
</div>
<form id="form-GETapi-v1-event" data-method="GET" data-path="api/v1/event" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-event', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-event" onclick="tryItOut('GETapi-v1-event');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-event" onclick="cancelTryOut('GETapi-v1-event');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-event" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/event</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-event" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-event" data-component="query"  hidden>
<br>
Event name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-event" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-event" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
Event Data sorted by start.
</p>
<h2>Get Event By ID</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Event By ID, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/event/quo?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/event/quo"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get Event Data",
    "data": null
}</code></pre>
<div id="execution-results-GETapi-v1-event--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-event--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-event--id-"></code></pre>
</div>
<div id="execution-error-GETapi-v1-event--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-event--id-"></code></pre>
</div>
<form id="form-GETapi-v1-event--id-" data-method="GET" data-path="api/v1/event/{id}" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-event--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-event--id-" onclick="tryItOut('GETapi-v1-event--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-event--id-" onclick="cancelTryOut('GETapi-v1-event--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-event--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/event/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-v1-event--id-" data-component="url" required  hidden>
<br>

</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-event--id-" data-component="query" required  hidden>
<br>
Authentication key.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Event Data obtained by id.
</p><h1>Event Presence</h1>
<p>APIs for managing Event Data</p>
<h2>Post Event Presence</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Post Event Presence, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/event_presence?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"event":1,"user":1,"name":"Herri","from":"Informatika","created_by":1}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/event_presence"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "event": 1,
    "user": 1,
    "name": "Herri",
    "from": "Informatika",
    "created_by": 1
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-event_presence" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-event_presence"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-event_presence"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-event_presence" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-event_presence"></code></pre>
</div>
<form id="form-POSTapi-v1-event_presence" data-method="POST" data-path="api/v1/event_presence" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-event_presence', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-event_presence" onclick="tryItOut('POSTapi-v1-event_presence');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-event_presence" onclick="cancelTryOut('POSTapi-v1-event_presence');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-event_presence" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/event_presence</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-event_presence" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="event" data-endpoint="POSTapi-v1-event_presence" data-component="body"  hidden>
<br>
Event id
</p>
<p>
<b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="user" data-endpoint="POSTapi-v1-event_presence" data-component="body" required  hidden>
<br>
User's id who attended the event
</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-v1-event_presence" data-component="body" required  hidden>
<br>
User's name who attended the event
</p>
<p>
<b><code>from</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="from" data-endpoint="POSTapi-v1-event_presence" data-component="body" required  hidden>
<br>
User's department who attended the event
</p>
<p>
<b><code>created_by</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="created_by" data-endpoint="POSTapi-v1-event_presence" data-component="body"  hidden>
<br>
User's id who attended the event
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Recently entered Event Precence data
</p><h1>Kepengurusan Management</h1>
<p>APIs for managing Kepengurusan Data</p>
<h2>Get Kepengurusan</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Kepengurusan Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/kepengurusan?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=id&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/kepengurusan"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "id",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get Kepengurusan Data",
    "data": []
}</code></pre>
<div id="execution-results-GETapi-v1-kepengurusan" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-kepengurusan"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-kepengurusan"></code></pre>
</div>
<div id="execution-error-GETapi-v1-kepengurusan" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-kepengurusan"></code></pre>
</div>
<form id="form-GETapi-v1-kepengurusan" data-method="GET" data-path="api/v1/kepengurusan" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-kepengurusan', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-kepengurusan" onclick="tryItOut('GETapi-v1-kepengurusan');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-kepengurusan" onclick="cancelTryOut('GETapi-v1-kepengurusan');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-kepengurusan" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/kepengurusan</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-kepengurusan" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-kepengurusan" data-component="query"  hidden>
<br>
Kepengurusan name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-kepengurusan" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-kepengurusan" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
Kepengurusan Data sorted by distance.
</p><h1>Konseling Management</h1>
<p>APIs for managing Konseling Data</p>
<h2>Get Konseling</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Konseling Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/konseling?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=maxime&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/konseling"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "maxime",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get Counseling Data",
    "data": []
}</code></pre>
<div id="execution-results-GETapi-v1-konseling" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-konseling"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-konseling"></code></pre>
</div>
<div id="execution-error-GETapi-v1-konseling" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-konseling"></code></pre>
</div>
<form id="form-GETapi-v1-konseling" data-method="GET" data-path="api/v1/konseling" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-konseling', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-konseling" onclick="tryItOut('GETapi-v1-konseling');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-konseling" onclick="cancelTryOut('GETapi-v1-konseling');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-konseling" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/konseling</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-konseling" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-konseling" data-component="query"  hidden>
<br>
Konseling name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-konseling" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-konseling" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
Konseling Data sorted by distance.
</p>
<h2>Post Konseling</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Post Konseling, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/konseling?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"user":1,"status":0,"type":1}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/konseling"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "user": 1,
    "status": 0,
    "type": 1
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-konseling" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-konseling"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-konseling"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-konseling" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-konseling"></code></pre>
</div>
<form id="form-POSTapi-v1-konseling" data-method="POST" data-path="api/v1/konseling" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-konseling', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-konseling" onclick="tryItOut('POSTapi-v1-konseling');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-konseling" onclick="cancelTryOut('POSTapi-v1-konseling');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-konseling" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/konseling</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-konseling" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="user" data-endpoint="POSTapi-v1-konseling" data-component="body" required  hidden>
<br>
User's id who created the Counseling
</p>
<p>
<b><code>status</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="status" data-endpoint="POSTapi-v1-konseling" data-component="body" required  hidden>
<br>
Counseling has been carried out or not
</p>
<p>
<b><code>type</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="type" data-endpoint="POSTapi-v1-konseling" data-component="body" required  hidden>
<br>
Type of Counseling
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Recently entered Konseling data
</p><h1>News Management</h1>
<p>APIs for managing News Data</p>
<h2>Get News</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get News Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/news?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=inventore&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/news"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "inventore",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get News Data",
    "data": []
}</code></pre>
<div id="execution-results-GETapi-v1-news" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-news"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-news"></code></pre>
</div>
<div id="execution-error-GETapi-v1-news" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-news"></code></pre>
</div>
<form id="form-GETapi-v1-news" data-method="GET" data-path="api/v1/news" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-news', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-news" onclick="tryItOut('GETapi-v1-news');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-news" onclick="cancelTryOut('GETapi-v1-news');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-news" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/news</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-news" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-news" data-component="query"  hidden>
<br>
News name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-news" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-news" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
News Data sorted by distance.
</p><h1>Notif Management</h1>
<p>APIs for managing Notif Data</p>
<h2>Get Notif</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Notif Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/notif/et?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=qui&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/notif/et"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "qui",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get Notif Data",
    "data": null
}</code></pre>
<div id="execution-results-GETapi-v1-notif--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-notif--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-notif--id-"></code></pre>
</div>
<div id="execution-error-GETapi-v1-notif--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-notif--id-"></code></pre>
</div>
<form id="form-GETapi-v1-notif--id-" data-method="GET" data-path="api/v1/notif/{id}" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-notif--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-notif--id-" onclick="tryItOut('GETapi-v1-notif--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-notif--id-" onclick="cancelTryOut('GETapi-v1-notif--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-notif--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/notif/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-v1-notif--id-" data-component="url" required  hidden>
<br>

</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-notif--id-" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-notif--id-" data-component="query"  hidden>
<br>
Notif name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-notif--id-" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-notif--id-" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Notif Data sorted by distance.
</p><h1>PrayerRequest Management</h1>
<p>APIs for managing PrayerRequest Data</p>
<h2>Get PrayerRequest</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get PrayerRequest Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/prayer_request?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=neque&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/prayer_request"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "neque",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get PrayerRequest Data",
    "data": []
}</code></pre>
<div id="execution-results-GETapi-v1-prayer_request" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-prayer_request"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-prayer_request"></code></pre>
</div>
<div id="execution-error-GETapi-v1-prayer_request" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-prayer_request"></code></pre>
</div>
<form id="form-GETapi-v1-prayer_request" data-method="GET" data-path="api/v1/prayer_request" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-prayer_request', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-prayer_request" onclick="tryItOut('GETapi-v1-prayer_request');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-prayer_request" onclick="cancelTryOut('GETapi-v1-prayer_request');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-prayer_request" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/prayer_request</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-prayer_request" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-prayer_request" data-component="query"  hidden>
<br>
PrayerRequest name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-prayer_request" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-prayer_request" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
PrayerRequest Data sorted by distance.
</p>
<h2>Post Prayer Request</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Post Prayer Request, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/prayer_request?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"user":1,"prayer":"Sidang  TA lancar dan lulus","report":"Ya bapa kami semoga TA teman kami berjalan lancar","visible":false,"showed":false,"prayed":false}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/prayer_request"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "user": 1,
    "prayer": "Sidang  TA lancar dan lulus",
    "report": "Ya bapa kami semoga TA teman kami berjalan lancar",
    "visible": false,
    "showed": false,
    "prayed": false
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-prayer_request" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-prayer_request"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-prayer_request"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-prayer_request" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-prayer_request"></code></pre>
</div>
<form id="form-POSTapi-v1-prayer_request" data-method="POST" data-path="api/v1/prayer_request" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-prayer_request', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-prayer_request" onclick="tryItOut('POSTapi-v1-prayer_request');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-prayer_request" onclick="cancelTryOut('POSTapi-v1-prayer_request');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-prayer_request" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/prayer_request</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-prayer_request" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="user" data-endpoint="POSTapi-v1-prayer_request" data-component="body"  hidden>
<br>
User's id who created the Prayer Request
</p>
<p>
<b><code>prayer</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="prayer" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required  hidden>
<br>
Things to pray for
</p>
<p>
<b><code>report</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="report" data-endpoint="POSTapi-v1-prayer_request" data-component="body"  hidden>
<br>
The content of the prayer of the person who prays
</p>
<p>
<b><code>visible</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<label data-endpoint="POSTapi-v1-prayer_request" hidden><input type="radio" name="visible" value="true" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required ><code>true</code></label>
<label data-endpoint="POSTapi-v1-prayer_request" hidden><input type="radio" name="visible" value="false" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required ><code>false</code></label>
<br>
Visible or not
</p>
<p>
<b><code>showed</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<label data-endpoint="POSTapi-v1-prayer_request" hidden><input type="radio" name="showed" value="true" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required ><code>true</code></label>
<label data-endpoint="POSTapi-v1-prayer_request" hidden><input type="radio" name="showed" value="false" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required ><code>false</code></label>
<br>
Showed or not
</p>
<p>
<b><code>prayed</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<label data-endpoint="POSTapi-v1-prayer_request" hidden><input type="radio" name="prayed" value="true" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required ><code>true</code></label>
<label data-endpoint="POSTapi-v1-prayer_request" hidden><input type="radio" name="prayed" value="false" data-endpoint="POSTapi-v1-prayer_request" data-component="body" required ><code>false</code></label>
<br>
Has been prayed or not
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Recently entered Prayer Request data
</p><h1>Renungan Management</h1>
<p>APIs for managing Renungan Data</p>
<h2>Get Renungan</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get Renungan Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/renungan?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=et&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/renungan"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "et",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true,
    "result": "Successfully Get Renungan Data",
    "data": [
        {
            "id": 2,
            "title": "Molestiae tempora eveniet.",
            "verse": "markus 10:11",
            "slug": "Atque et voluptate autem exercitationem rerum omnis.",
            "source": "kitabsuci",
            "qr": null,
            "description": "Earum sit facere in qui assumenda sit. Porro illo est velit adipisci sit.",
            "excerpt": "Corporis sit consequatur quod ad neque.",
            "tag": null,
            "cover": null,
            "date": "1985-12-16T17:00:00.000000Z",
            "read": 0,
            "kepengurusan": 1,
            "created_by": 1,
            "created_at": "2022-01-25T06:36:52.000000Z",
            "updated_at": "2022-01-25T06:36:52.000000Z"
        }
    ]
}</code></pre>
<div id="execution-results-GETapi-v1-renungan" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-renungan"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-renungan"></code></pre>
</div>
<div id="execution-error-GETapi-v1-renungan" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-renungan"></code></pre>
</div>
<form id="form-GETapi-v1-renungan" data-method="GET" data-path="api/v1/renungan" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-renungan', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-renungan" onclick="tryItOut('GETapi-v1-renungan');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-renungan" onclick="cancelTryOut('GETapi-v1-renungan');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-renungan" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/renungan</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-renungan" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-renungan" data-component="query"  hidden>
<br>
Renungan name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-renungan" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-renungan" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;<small>boolean</small>  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;<small>array</small>  &nbsp;
<br>
Renungan Data sorted by distance.
</p><h1>User Management</h1>
<p>APIs for managing User Data</p>
<h2>Get User List</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get User List, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/users?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=cumque&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/users"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "cumque",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (500):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Auth guard [api] is not defined.",
    "exception": "InvalidArgumentException",
    "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\AuthManager.php",
    "line": 84,
    "trace": [
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\AuthManager.php",
            "line": 68,
            "function": "resolve",
            "class": "Illuminate\\Auth\\AuthManager",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
            "line": 63,
            "function": "guard",
            "class": "Illuminate\\Auth\\AuthManager",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
            "line": 42,
            "function": "authenticate",
            "class": "Illuminate\\Auth\\Middleware\\Authenticate",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\Authenticate",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 697,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 672,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ConvertEmptyStringsToNull.php",
            "line": 31,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ConvertEmptyStringsToNull",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TrimStrings.php",
            "line": 40,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TrimStrings",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php",
            "line": 52,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Middleware\\TrustProxies.php",
            "line": 39,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Http\\Middleware\\TrustProxies",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php",
            "line": 651,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Command\\Command.php",
            "line": 299,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Application.php",
            "line": 978,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Application.php",
            "line": 295,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Application.php",
            "line": 167,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "-&gt;"
        }
    ]
}</code></pre>
<div id="execution-results-GETapi-v1-users" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-users"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-users"></code></pre>
</div>
<div id="execution-error-GETapi-v1-users" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-users"></code></pre>
</div>
<form id="form-GETapi-v1-users" data-method="GET" data-path="api/v1/users" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-users', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-users" onclick="tryItOut('GETapi-v1-users');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-users" onclick="cancelTryOut('GETapi-v1-users');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-users" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/users</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-users" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-users" data-component="query"  hidden>
<br>
User name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-users" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-users" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User Data sorted by distance.
</p>
<h2>Get User Data</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get User Data, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost:8000/api/v1/users/doloribus?api_token=%7BYOUR_AUTH_KEY%7D&amp;search=quibusdam&amp;limit=3&amp;page=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/users/doloribus"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
    "search": "quibusdam",
    "limit": "3",
    "page": "1",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (500):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Auth guard [api] is not defined.",
    "exception": "InvalidArgumentException",
    "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\AuthManager.php",
    "line": 84,
    "trace": [
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\AuthManager.php",
            "line": 68,
            "function": "resolve",
            "class": "Illuminate\\Auth\\AuthManager",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
            "line": 63,
            "function": "guard",
            "class": "Illuminate\\Auth\\AuthManager",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
            "line": 42,
            "function": "authenticate",
            "class": "Illuminate\\Auth\\Middleware\\Authenticate",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\Authenticate",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 697,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 672,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ConvertEmptyStringsToNull.php",
            "line": 31,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ConvertEmptyStringsToNull",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TrimStrings.php",
            "line": 40,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TrimStrings",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php",
            "line": 52,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Middleware\\TrustProxies.php",
            "line": 39,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Http\\Middleware\\TrustProxies",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php",
            "line": 651,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Command\\Command.php",
            "line": 299,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Application.php",
            "line": 978,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Application.php",
            "line": 295,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\symfony\\console\\Application.php",
            "line": 167,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "-&gt;"
        },
        {
            "file": "D:\\HERRI\\kp\\website-PMK\\pmkits\\artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "-&gt;"
        }
    ]
}</code></pre>
<div id="execution-results-GETapi-v1-users--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-v1-users--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-users--id-"></code></pre>
</div>
<div id="execution-error-GETapi-v1-users--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-users--id-"></code></pre>
</div>
<form id="form-GETapi-v1-users--id-" data-method="GET" data-path="api/v1/users/{id}" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-users--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-v1-users--id-" onclick="tryItOut('GETapi-v1-users--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-v1-users--id-" onclick="cancelTryOut('GETapi-v1-users--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-v1-users--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/v1/users/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-v1-users--id-" data-component="url" required  hidden>
<br>

</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="GETapi-v1-users--id-" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-v1-users--id-" data-component="query"  hidden>
<br>
User name to be searched.
</p>
<p>
<b><code>limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="limit" data-endpoint="GETapi-v1-users--id-" data-component="query"  hidden>
<br>
Number of data shown.
</p>
<p>
<b><code>page</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="page" data-endpoint="GETapi-v1-users--id-" data-component="query"  hidden>
<br>
Selected page to be shown.
</p>
</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
User Data sorted by distance.
</p>
<h2>Put User</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Put User, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost:8000/api/v1/users/et?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"Herri","name":"Herri Purba","password":"Herri","role":"2","access":"Dope"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/users/et"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "Herri",
    "name": "Herri Purba",
    "password": "Herri",
    "role": "2",
    "access": "Dope"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-PUTapi-v1-users--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-v1-users--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-v1-users--id-"></code></pre>
</div>
<div id="execution-error-PUTapi-v1-users--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-v1-users--id-"></code></pre>
</div>
<form id="form-PUTapi-v1-users--id-" data-method="PUT" data-path="api/v1/users/{id}" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-v1-users--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-v1-users--id-" onclick="tryItOut('PUTapi-v1-users--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-v1-users--id-" onclick="cancelTryOut('PUTapi-v1-users--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-v1-users--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/v1/users/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="PUTapi-v1-users--id-" data-component="url" required  hidden>
<br>

</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="PUTapi-v1-users--id-" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>username</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="username" data-endpoint="PUTapi-v1-users--id-" data-component="body" required  hidden>
<br>
Unique username of the user who want to created
</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="PUTapi-v1-users--id-" data-component="body" required  hidden>
<br>
Full name of the user who want to created
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password" data-endpoint="PUTapi-v1-users--id-" data-component="body" required  hidden>
<br>
Password of the user who want to created
</p>
<p>
<b><code>role</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="role" data-endpoint="PUTapi-v1-users--id-" data-component="body" required  hidden>
<br>
Role of the user who want to created
</p>
<p>
<b><code>access</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="access" data-endpoint="PUTapi-v1-users--id-" data-component="body"  hidden>
<br>
Access of the user who want to created
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Recently entered User data
</p>
<h2>Post User</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Post User, on success you'll get a 200 OK response.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost:8000/api/v1/users/register?api_token=%7BYOUR_AUTH_KEY%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"Herri","name":"Herri Purba","password":"Herri","role":"2","access":"Dope"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost:8000/api/v1/users/register"
);

let params = {
    "api_token": "{YOUR_AUTH_KEY}",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "Herri",
    "name": "Herri Purba",
    "password": "Herri",
    "role": "2",
    "access": "Dope"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-v1-users-register" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-v1-users-register"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-users-register"></code></pre>
</div>
<div id="execution-error-POSTapi-v1-users-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-users-register"></code></pre>
</div>
<form id="form-POSTapi-v1-users-register" data-method="POST" data-path="api/v1/users/register" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-users-register', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-v1-users-register" onclick="tryItOut('POSTapi-v1-users-register');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-v1-users-register" onclick="cancelTryOut('POSTapi-v1-users-register');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-v1-users-register" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/v1/users/register</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>api_token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="api_token" data-endpoint="POSTapi-v1-users-register" data-component="query" required  hidden>
<br>
Authentication key.
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>username</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="username" data-endpoint="POSTapi-v1-users-register" data-component="body" required  hidden>
<br>
Unique username of the user who want to created
</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-v1-users-register" data-component="body" required  hidden>
<br>
Full name of the user who want to created
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password" data-endpoint="POSTapi-v1-users-register" data-component="body" required  hidden>
<br>
Password of the user who want to created
</p>
<p>
<b><code>role</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="role" data-endpoint="POSTapi-v1-users-register" data-component="body" required  hidden>
<br>
Role of the user who want to created
</p>
<p>
<b><code>access</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="access" data-endpoint="POSTapi-v1-users-register" data-component="body"  hidden>
<br>
Access of the user who want to created
</p>

</form>
<h3>Response</h3>
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>success</code></b>&nbsp;&nbsp;  &nbsp;
<br>
The status of this API request.
</p>
<p>
<b><code>result</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Description of this API request.
</p>
<p>
<b><code>data</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Recently entered User data
</p>
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var languages = ["bash","javascript"];
        setupLanguages(languages);
    });
</script>
</body>
</html>
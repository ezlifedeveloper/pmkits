@extends('admin.app', ['title' => 'Event', 'menu' => 'event'])

@section('content')

<div class="row">
    <div class="col-12" style="margin-bottom: 10px">
        <a href="{{route('adminevent')}}" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</a>
    </div>

    <div class="col-lg-3">
        @livewire('admin.event-detail-component', ['eventid' => Request::route('id')], key('detailevent'))
    </div>

    <div class="col-lg-3">
        @livewire('participant-component', ['eventid' => Request::route('id')], key('participant'))
    </div>

    <div class="col-lg-6">
        @livewire('admin.gallery-component', ['eventid' => Request::route('id')], key('gallery'))
    </div>
</div>

@endsection

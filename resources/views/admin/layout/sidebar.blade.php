<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 text-sm">
    <!-- Brand Logo -->
    <a href="{{url("/")}}" class="brand-link">
        <img src="{{asset('PMKITS.png')}}" alt="App Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">PMK ITS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
            <img src="{{asset('admin/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
            <a href="#" class="d-block">{{Auth::user()->name ?? "ADMINISTRATOR"}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
                <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link @if($title=='Dashboard') active @endif">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p> Dashboard </p>
                    </a>
                </li>
                @if (Auth::user()->role == 1 ||(Auth::user()->role == 2 && str_contains(Auth::user()->access,'Dope')))
                <li class="nav-header">User</li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-item">
                    <a href="{{route('adminuser')}}" class="nav-link @if($title=='User') active @endif">
                    <i class="nav-icon fas fa-users"></i>
                        <p> User </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1 ||(Auth::user()->role == 2 && str_contains(Auth::user()->access,'Dope')))
                <li class="nav-item">
                    <a href="{{route('adminprayerrequest')}}" class="nav-link @if($title=='Prayer Request') active @endif">
                    <i class="nav-icon fas fa-cross"></i>
                        <p> Prayer Request </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-item">
                    <a href="{{route('adminriwayatorganisasi')}}" class="nav-link @if($title=='Riwayat Organisasi') active @endif">
                    <i class="nav-icon fas fa-certificate"></i>
                        <p> Riwayat Organisasi </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1 ||(Auth::user()->role == 2 && str_contains(Auth::user()->access,'Dope')))
                <li class="nav-item">
                    <a href="{{route('adminkonseling')}}" class="nav-link @if($title=='Konseling') active @endif">
                    <i class="nav-icon fab fa-meetup"></i>
                    {{-- <i class="fas fa-head-side-medical"></i> --}}
                        <p> Konseling </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1 ||(Auth::user()->role == 2 && str_contains(Auth::user()->access,'Bendahara')))
                <li class="nav-header">Informasi</li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-item">
                    <a href="{{route('adminevent')}}" class="nav-link @if($title=='Event') active @endif">
                    <i class="nav-icon fas fa-calendar"></i>
                        <p> Event </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-item">
                    <a href="{{route('adminnews')}}" class="nav-link @if($title=='News') active @endif">
                    <i class="nav-icon fas fa-newspaper"></i>
                        <p> News </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-item">
                    <a href="{{route('adminrenungan')}}" class="nav-link @if($title=='Renungan') active @endif">
                    <i class="nav-icon fas fa-book"></i>
                        <p> Renungan </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1 || (Auth::user()->role == 2 && str_contains(Auth::user()->access,'Bendahara')))
                <li class="nav-item">
                    <a href="{{route('adminkepengurusan')}}" class="nav-link @if($title=='Laporan Keuangan') active @endif">
                    <i class="nav-icon fas fa-coins"></i>
                        <p> Laporan Keuangan </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-header">Profil PMK</li>
                <li class="nav-item">
                    <a href="{{route('adminprofil')}}" class="nav-link @if($title=='Profil') active @endif">
                    <i class="nav-icon fas fa-eye"></i>
                        <p> Profil </p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 1)
                <li class="nav-item">
                    <a href="{{route('adminkepengurusan')}}" class="nav-link @if($title=='Kepengurusan') active @endif">
                    <i class="nav-icon fas fa-briefcase"></i>
                        <p> Kepengurusan </p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

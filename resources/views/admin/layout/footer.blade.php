<footer class="main-footer">
    2021 @if(date('Y')>2021)-{{date('Y')}}@endif <strong>Copyright &copy; {{env('APP_NAME')}} v{{env('APP_VERSION')}}
</footer>

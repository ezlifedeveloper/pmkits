<div>
    <section class="bg-gray" id="blog">
        <div class="container">
            <div class="row" style="padding-bottom: 0px; margin-bottom: 0px; padding-top:-20px">
                <div class="col-sm-4 col-sm-offset-4" style="text-align: center">
                    <form class="form-inline subscribe-form" action="#" style="margin-bottom: 15px" method="post">
                        <div class="input-group input-group-lg" style="width: 100%">
                            <input class="form-control" wire:model='searchData' type="search" name="search" placeholder="Search..."><span class="input-group-btn">
                            <button class="btn btn-dark" type="submit" name="search"><i class="fa fa-search fa-lg"></i></button></span>
                        </div>
                    </form>
                    @include('web.layout.tablecountinfo')
                </div>
            </div>
            @foreach ($lists as $d)
                @if($loop->iteration%3==0)<div class="row grid-pad" style="padding-top: 0px">@endif
                <div class="col-sm-4">
                    <a href="{{route('news.detail', $d->slug)}}">
                        <img class="img-responsive center" height="250px" style="margin-bottom:10px" src="{{$d->cover}}" alt="{{$d->title}}">
                        <h4>
                            {{$d->title}}<br/>
                            <small>{{$d->created_at->diffForHumans()}}</small>
                        </h4>
                    </a>
                    <p style="text-align: justify">{{$d->excerpt}}</p>
                </div>
                @if($loop->iteration%3==0)</div>@endif
            @endforeach
            <div class="row">
                <div class="col-lg-12 text-center">
                    @if($lists->hasPages())
                        {{ $lists->links() }}
                    @endif
                </div>
            </div>

            @livewire('nav-footer-component', ['menu' => $menu, 'state' => 'list'])
        </div>
    </section>
</div>

@if(($data->show && $data->open->greaterThanOrEqualTo($now)) || (!$data->show && $data->start->greaterThanOrEqualTo($now)))
<div wire:poll.1000ms='countdown'>
    <h2 class="bold">
        {{$detail}}<br/>
        {{$data->title}}<br/>
    </h2>
    @if(strlen($data->speaker)>0){{$data->speaker}}<br/>@endif
    @if(strlen($data->verse)>0){{$data->verse}}<br/><br/>@endif
    @if(strlen($data->description)>0){!! $data->description !!}<br/>@endif
    @if($isAbsenced) <h4 style="margin: 2vh 0vh">Anda telah melakukan absensi</h4> @endif
    <div class="no-pad" id="clock">
        <div><span>{{$days}}</span>days</div>
        <div><span>{{$hours}}</span>hr</div>
        <div><span>{{$mins}}</span>min</div>
        <div><span>{{$secs}}</span>sec</div>
    </div>
</div>
@else
<div wire:poll.1000ms='countdown' class="row" style="margin-bottom: 2vh; padding-top: 0px">
    <h2 class="bold">
        {{$data->name}}<br/>
        {{$data->title}}<br/>
    </h2>
    @if(strlen($data->speaker)>0){{$data->speaker}}<br/>@endif
    @if(strlen($data->verse)>0){{$data->verse}}<br/>@endif
    @if(strlen($data->description)>0){!! $data->description !!}<br/>@endif
    @if($data->show && $data->open->lessThan($now) && $data->close->greaterThanOrEqualTo($now))
        <h3 style="margin: 2vh 0vh">Selamat Datang!</h3>
        {{-- @if(!$role) --}}
            @if($isAbsenced) <h4 style="margin: 2vh 0vh">Anda telah melakukan absensi</h4>
            @else
                @if($mobile)
                    @if($username)
                    <a class="btn btn-primary" target="_blank" href="{{env('APP_URL').'/attend/'.$data->slug.'/'.$username.'/'.$admin.'/1/'.$iduser}}">
                        {{$data->absence_type}} Mobile @if($data->url) dan Masuk Room @endif
                    </a>
                    @else
                    <a class="btn btn-primary" target="_blank" href="{{env('APP_URL').'/attend/'.$data->slug}}">
                        {{$data->absence_type}} Mobile @if($data->url) dan Masuk Room @endif
                    </a>
                    @endif
                @else
                    @if(Auth::check())
                    <a class="btn btn-primary" target="_blank" href="{{env('APP_URL').'/attend/'.$data->slug.'/'.Auth::user()->username}}">
                        {{$data->absence_type}} @if($data->url) dan Masuk Room @endif
                    </a>
                    @else
                    <a class="btn btn-primary" target="_blank" href="{{env('APP_URL').'/attend/'.$data->slug}}">
                        {{$data->absence_type}} @if($data->url) dan Masuk Room @endif
                    </a>
                    @endif
                @endif
            @endif
        {{-- @endif --}}
    @elseif($data->end->greaterThanOrEqualTo($now))
        <h3 style="margin: 2vh 0vh">Kegiatan Sedang Berlangsung<br/>Selamat Mengikuti Kegiatan!<br/>Tuhan memberkati!</h3>
    @else
    <h3 style="margin: 2vh 0vh">Kegiatan Telah Selesai Dilaksanakan</h3>
    @endif
</div>
@endif

<div>
    <!-- About-->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Prayer Request</h2>
                    <p style="margin-bottom: 10px">Since the Word of God is living and active, we provide a dedicated team of anointed prayer ministers to support you in your prayer request, as you lift your voice in prayer Monday through Friday, from 9 a.m. to 5 p.m. (EST). The Bible tells us that if anyone is a worshipper of God and does His will, God listens to him (John 9:31), and “if we ask anything according to His will, He hears us” (1 John 5:14).</p>
                </div>
                <div class="col-lg-5 col-lg-offset-1" style="display: inline-block; height: 100%;">
                    <h3>&nbsp;</h3>
                    <div class="row text-center" style="padding:0" wire:ignore>
                        <textarea wire:model='input_prayer' style="padding: 30px" id="input_prayer" class="form-control" rows="4" required></textarea>
                        @error('input_prayer') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        <a wire:click='store()' class="btn btn-primary" style="margin-top: 20px">Kirim Prayer Request</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-gray" id="blog">
        <div class="container">
            <h2>Mari kita doakan</h2>
            <div class="row" style="padding-bottom: 0px; margin-bottom: 0px; padding-top:-20px" wire:poll>
                @foreach ($lists as $d)
                    <div class="col-md-3 col-sm-6 col-xs-12" style="height: 150px">
                        <small style="float: left"><i class="ion-ios-clock-outline icon-small" style="margin-right: 5px"></i>{{$d->created_at}}</small><br/>
                        <blockquote class="no-pad">{{ $d->prayer }}</blockquote>
                    </div>
                @endforeach
            </div>
            <h2>Prayer Request Anda</h2>
            <div class="row" style="padding-bottom: 0px; margin-bottom: 0px; padding-top:-20px" wire:poll>
                @foreach ($lists2 as $data)
                    <div class="col-md-12 col-sm-12 col-xs-12" style="height: auto">
                        <small style="float: left"><i class="ion-ios-clock-outline icon-small" style="margin-right: 5px"></i>{{$d->created_at}}</small><br/>
                        <blockquote class="no-pad"><strong>{{ $data->prayer }}</strong></blockquote>
                        <blockquote class="no-pad">{{ $data->report }}</blockquote>
                        @if ($data->prayed == 0)
                            <blockquote class="no-pad">Belum Didoakan</blockquote>
                        @elseif ($data->prayed == 1)
                            <blockquote class="no-pad">Sudah Didoakan</blockquote>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</div>

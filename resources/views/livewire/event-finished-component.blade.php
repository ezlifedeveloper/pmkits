<div class="row" style="margin-bottom: 2vh; padding-top: 0px">
    <h2 class="bold">{{$data->name}} - {{$data->title}}</h2>
    <h3 style="margin: 2vh 0vh">Kegiatan Telah Selesai Dilaksanakan</h3>

    <div class="col-lg-6">
        <p>{{$data->speaker}}<br/>{{$data->verse}}<br/>{{$data->start->format('d M Y H:i')}}</p>
        <p class="text-description" style="text-align: center">{!! $data->description !!}</p>
        <p class="text-description" style="text-align: center">{!! $data->report !!}</p>
    </div>
    <div class="col-lg-6">
        <div class="row">
            @foreach ($gallery as $d)
            <div class="col-sm-4">
                <img src="{{$d->file}}" alt="{{$d->parent->name}}" style="height: 100px; width:100%; margin:5px">
            </div>
            @endforeach
        </div>
    </div>
</div>

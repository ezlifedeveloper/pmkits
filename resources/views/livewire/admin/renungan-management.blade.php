<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data Renungan</h3>
                <button class="btn btn-xs btn-light" wire:click="createData()" style="float: right; margin: 0px 0px 10px 10px">Create Data</button>
                {{-- <button wire:click="openImport()" class="btn btn-xs btn-primary" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-download pr-1"></i> Import</button>
                <button wire:click="exportExcel()" class="btn btn-xs btn-success" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-upload pr-1"></i> Export</button> --}}
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>Tanggal</th>
                            <th>Judul</th>
                            <th>Link</th>
                            <th>Deskripsi</th>
                            <th>Dilihat</th>
                            <th width="150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->date->format('d M Y')}}</td>
                            <td>{{$d->title}}</td>
                            <td><a target="_blank" href="{{env('APP_URL').'/renungan/'.$d->slug}}">Renungan</a></td>
                            <td>{{$d->excerpt}}</td>
                            <td>
                                <small class="text-secondary mr-1">
                                    <i class="fas fa-eye"></i>
                                    {{$d->read ?? 0}}
                                </small>
                            </td>
                            <td>
                                <button wire:click="openData({{ $d->id }})" title="Lihat Detail Renungan" class="btn btn-xs btn-info icon-button">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" title="Hapus Renungan" class="btn btn-xs btn-danger icon-button" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="7">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
    @if($showData)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"></h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <form method="post" wire:submit.prevent="storeData()">
            <div class="card-body">
                <div class="form-group col-12">
                    <label for="input_title">Judul Renungan</label>
                    <input type="text" wire:model="input_title" id="input_title" class="form-control @error('input_title') is-invalid @enderror" required>
                    @error('input_title') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_verse">Ayat Renungan</label>
                    <input type="text" wire:model="input_verse" id="input_verse" class="form-control @error('input_verse') is-invalid @enderror" required>
                    @error('input_verse') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_source">Sumber / Pembuat Renungan</label>
                    <input type="text" wire:model="input_source" id="input_source" class="form-control @error('input_source') is-invalid @enderror" required>
                    @error('input_source') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_description">Renungan</label>
                    <textarea wire:model="input_description" id="input_description" class="form-control @error('input_description') is-invalid @enderror" rows="5" required></textarea>
                    @error('input_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label class="font-weight-bold">Tanggal Renungan</label>
                    <input type="date" wire:model="input_date" class="form-control @error('input_date') is-invalid @enderror">
                    @error('input_date') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_tag">Tag (Separated by , )</label>
                    <input type="text" wire:model="input_tag" id="input_tag" class="form-control @error('input_tag') is-invalid @enderror">
                    @error('input_tag') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="cover">Cover Image</label>
                    <input type="file" wire:model="cover" class="form-control">
                    @error('cover') <span class="error">{{ $message }}</span> @enderror
                    @if ($cover)
                    Photo Preview:
                    <img src="{{ !is_string($cover) ? $cover->temporaryUrl() : $cover }}" width="100%">
                    @endif
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
    @endif
    </div>
</div>

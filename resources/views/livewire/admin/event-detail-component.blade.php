<div>
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Detail Acara</h3>
            </div>
            <div class="card-body">
                @if($event && $event->hdcover)
                <div class="row">
                    <div class="col-12">
                        <img src="{{$event->hdcover}}" style="width: 100%"/>
                    </div>
                </div>
                @endif
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-6">
                        <img src="{{$event->cover}}" style="width: 100%"/>
                    </div>
                    <div class="col-sm-6">
                        <p style="margin-bottom: 5px">
                            <b>Nama Event</b><br/>{{$event->name}}
                        </p>
                        <p style="margin-bottom: 5px">
                            <b>Pelaksanaan</b><br/>{{$event->time}}
                        </p>
                        <p style="margin-bottom: 5px">
                            <b>Tag</b><br/>{{$event->tag}}
                        </p>
                        <p style="margin-bottom: 5px">
                            <b>Peserta</b><br/>{{$event->participant}}
                        </p>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-6">
                        <b>Deskripsi</b>
                        <p class="text-description">{{ $event->description }}</p>
                    </div>
                    <div class="col-sm-6">
                        <img src="{{$event->qr}}" style="width: 100%"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Laporan Selesai Acara</h3>
            </div>
            <div class="card-body">
                @if(!$event->report || $this->editReport)
                <form method="post">
                    <div class="form-group col-12">
                        <textarea wire:ignore wire:model="input_report" id="input_report" class="form-control @error('input_report') is-invalid @enderror" rows="10" required>{{$input_report}}</textarea>
                        @error('input_report') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                </form>
                @else
                <p class="text-description">{!!$event->report!!}</p>
                @endif
            </div>
            <div class="card-footer text-right">
                @if(!$event->report || $this->editReport)
                <button type="button" wire:click="storeReport({{$event->id}})" class="btn btn-success">Save</button>
                @else
                <button type="button" wire:click="openReport({{$event->id}})" class="btn btn-success">Edit</button>
                @endif
            </div>
        </div>
</div>

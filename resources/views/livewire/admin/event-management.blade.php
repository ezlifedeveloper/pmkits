<div class="row">
    <div class="col-lg-7">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data Event</h3>
                <button class="btn btn-xs btn-light" wire:click="createEvent()" style="float: right; margin: 0px 0px 10px 10px">Create Event</button>
                {{-- <button wire:click="openImport()" class="btn btn-xs btn-primary" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-download pr-1"></i> Import</button> --}}
                {{-- <button wire:click="exportExcel()" class="btn btn-xs btn-success" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-upload pr-1"></i> Export</button> --}}
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 5%">#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Link</th>
                            <th>Peserta</th>
                            <th>Report</th>
                            <th>Dilihat</th>
                            <th width="120px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr class="text-center">
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->start}}<br/>to<br/>{{$d->end}}</td>
                            <td>
                                @if($d->access_link)<a target="_blank" href="{{$d->access_link}}">Event</a><br/>@endif
                                @if($d->absence_link)<a target="_blank" href="{{$d->absence_link}}">Absensi</a><br/>@endif
                                @if($d->url)<a target="_blank" href="{{$d->url}}">Zoom</a><br/>@endif
                            </td>
                            <td>{{$d->participant}}</td>
                            <td>
                                @if($d->report)
                                <small class="text-success mr-1">
                                    <i class="fas fa-check"></i>
                                </small>
                                @else
                                <small class="text-danger mr-1">
                                    <i class="fas fa-times"></i>
                                </small>
                                @endif
                            </td>
                            <td>
                                <small class="text-secondary mr-1">
                                    <i class="fas fa-eye"></i>
                                    {{$d->read ?? 0}}
                                </small>
                            </td>
                            <td>
                                {{-- <button wire:click="addReport({{ $d->id }})" class="btn btn-xs btn-success" title="Buat Laporan Selesai Event" style="width:30px; height:30px; margin: 1px">
                                    <i class="fas fa-check"></i>
                                </button> --}}
                                <a href={{route('admineventdetail', $d->id)}} class="btn btn-xs btn-primary icon-button" title="Lihat Detail Event">
                                    <i class="fas fa-eye"></i>
                                </a>
                                <button wire:click="openEvent({{ $d->id }})" class="btn btn-xs btn-info icon-button" title="Edit Detail Event">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" class="btn btn-xs btn-danger icon-button" title="Hapus Event" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="4">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links()}}
                @endif
                </div>
            </div>
        </div>
    </div>
    @if($showEvent)
    <div class="col-lg-5">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title"></h3>
                <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
            </div>
            <form method="post" wire:submit.prevent="storeEvent()">
                <div class="card-body">
                    <div class="form-group col-12">
                        <label for="input_name">Nama Kegiatan</label>
                        <input type="text" wire:model="input_name" id="input_name" class="form-control @error('input_name') is-invalid @enderror" required>
                        @error('input_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_description">Deskripsi Singkat</label>
                        <textarea wire:model="input_description" id="input_description" class="form-control @error('input_description') is-invalid @enderror" rows="5" required></textarea>
                        @error('input_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_title">Tema / Judul Kegiatan</label>
                        <input type="text" wire:model="input_title" id="input_title" class="form-control @error('input_title') is-invalid @enderror" required>
                        @error('input_title') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_location">Tempat / Lokasi Kegiatan</label>
                        <input type="text" wire:model="input_location" id="input_location" class="form-control @error('input_location') is-invalid @enderror" required>
                        @error('input_location') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_speaker">Nama Pembicara</label>
                        <input type="text" wire:model="input_speaker" id="input_speaker" class="form-control @error('input_speaker') is-invalid @enderror" required>
                        @error('input_speaker') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_verse">Ayat Alkitab</label>
                        <input type="text" wire:model="input_verse" id="input_verse" class="form-control @error('input_verse') is-invalid @enderror" required>
                        @error('input_verse') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_url">Link Zoom / Youtube (jika ada)</label>
                        <input type="text" wire:model="input_url" id="input_url" class="form-control @error('input_url') is-invalid @enderror">
                        @error('input_url') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_tag">Tag (Separated by , )</label>
                        <input type="text" wire:model="input_tag" id="input_tag" class="form-control @error('input_tag') is-invalid @enderror">
                        @error('input_tag') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-left:0; margin-right:0">
                            <div class="col-lg-6">
                                <label for="input_start">Mulai Acara</label>
                                <input type="datetime-local" wire:model="input_start" id="input_start" class="form-control @error('input_start') is-invalid @enderror" required>
                                @error('input_start') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                            <div class="col-lg-6">
                                <label for="input_end">Selesai Acara</label>
                                <input type="datetime-local" wire:model="input_end" id="input_end" class="form-control @error('input_end') is-invalid @enderror" required>
                                @error('input_end') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-left:0; margin-right:0">
                            <div class="col-lg-6">
                                <label for="input_open">Absensi Dibuka</label>
                                <input type="datetime-local" wire:model="input_open" id="input_open" class="form-control @error('input_open') is-invalid @enderror">
                                @error('input_open') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                            <div class="col-lg-6">
                                <label for="input_close">Absensi Ditutup</label>
                                <input type="datetime-local" wire:model="input_close" id="input_close" class="form-control @error('input_close') is-invalid @enderror">
                                @error('input_close') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <label for="input_show">Presensi ditampilkan secara umum di website?</label>
                        <select wire:model="input_show" class="form-control select2 @error('input_show') is-invalid @enderror" required>
                            <option value="1">YA</option>
                            <option value="0">TIDAK</option>
                        </select>
                        @error('input_show') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_absence_type">Jenis Presensi</label>
                        <select wire:model="input_absence_type" class="form-control select2 @error('input_absence_type') is-invalid @enderror" required>
                            <option>Absensi</option>
                            <option>Registrasi</option>
                        </select>
                        @error('input_absence_type') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_transfer">Perlu transfer?</label>
                        <select wire:model="input_transfer" class="form-control select2 @error('input_transfer') is-invalid @enderror" required>
                            <option value="0">TIDAK</option>
                            <option value="1">YA</option>
                        </select>
                        @error('input_transfer') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="cover">Cover Image</label>
                        <input type="file" wire:model="cover" class="form-control">
                        @error('cover') <span class="error">{{ $message }}</span> @enderror
                        @if ($cover)
                        Photo Preview:
                        <img src="{{ !is_string($cover) ? $cover->temporaryUrl() : $cover }}" width="100%">
                        @endif
                    </div>
                    <div class="form-group col-12">
                        <label for="hdcover">Cover Banner HD</label>
                        <input type="file" wire:model="hdcover" class="form-control">
                        @error('hdcover') <span class="error">{{ $message }}</span> @enderror
                        @if ($hdcover)
                        Photo Preview:
                        <img src="{{ !is_string($hdcover) ? $hdcover->temporaryUrl() : $hdcover }}" width="100%">
                        @endif
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
    @endif
</div>

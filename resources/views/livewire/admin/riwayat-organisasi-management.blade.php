<div class="row">
    <div wire:ignore.self class="modal fade" id="certificateModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="certificateModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" wire:submit.prevent="storeCertificate()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="certificateModalLabel">Upload Sertifikat</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_certificate">Sertifikat</label>
                            <input type="file" wire:model="input_certificate" class="form-control">
                            @error('input_certificate') <span class="error">{{ $message }}</span> @enderror
                            @if ($input_certificate)
                            Photo Preview:
                            <img src="{{ !is_string($input_certificate) ? $input_certificate->temporaryUrl() : $input_certificate }}" width="100%">
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="closeAll()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" wire:click.prevent="storeCertificate()" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="organisasiModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="organisasiModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" wire:submit.prevent="storeOrganisasi()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="organisasiModalLabel">Rekam Riwayat Organisasi</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_user">User</label>
                            <select wire:model="input_user" class="form-control @error('input_user') is-invalid @enderror" required>
                                <option value="" selected>-- Pilih User --</option>
                                @foreach ($user as $u)
                                <option value="{{$u->id}}">{{$u->username.' - '.$u->name}}</option>
                                @endforeach
                            </select>
                            @error('input_user') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputKepengurusan">Kepengurusan</label>
                            <select wire:model="inputKepengurusan" class="form-control @error('inputKepengurusan') is-invalid @enderror" required>
                                <option value="" selected>-- Pilih Kepengurusan --</option>
                                @foreach ($kepengurusan as $k)
                                <option value="{{$k->id}}">{{'PMK ITS '.$k->name}}</option>
                                @endforeach
                            </select>
                            @error('inputKepengurusan') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_start">Mulai</label>
                            <input type="date" wire:model="input_start" id="input_start" class="form-control @error('input_start') is-invalid @enderror" required>
                            @error('input_start') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_end">Selesai</label>
                            <input type="date" wire:model="input_end" id="input_end" class="form-control @error('input_end') is-invalid @enderror" required>
                            @error('input_end') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_position">Posisi</label>
                            <input type="text" wire:model="input_position" id="input_position" class="form-control @error('input_position') is-invalid @enderror" required>
                            @error('input_position') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_description">Deskripsi</label>
                            <input type="text" wire:model="input_description" id="input_description" class="form-control @error('input_description') is-invalid @enderror">
                            @error('input_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="closeAll()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" wire:click.prevent="storeOrganisasi()" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data Riwayat Organisasi</h3>
                <button class="btn btn-xs btn-light" data-toggle="modal" data-target="#organisasiModal" style="float: right; margin: 0px 0px 10px 10px">Create Riwayat Organisasi</button>
                {{-- <button wire:click="openImport()" class="btn btn-xs btn-primary" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-download pr-1"></i> Import</button> --}}
                {{-- <button wire:click="exportExcel()" class="btn btn-xs btn-success" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-upload pr-1"></i> Export</button> --}}
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>Kepengurusan</th>
                            <th>User</th>
                            <th>Periode</th>
                            <th>Posisi</th>
                            <th>Approved</th>
                            <th>Sertifikat</th>
                            <th width="120px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->parent->name}}</td>
                            <td>{{$d->creator->name}}</td>
                            <td>{{ $d->start->format('d M Y').' - '.($d->finish ? $d->finish->format('d M Y') : 'now') }}</td>
                            <td>{{$d->position}}<br/>{{$d->description}}</td>
                            <td>
                                @if($d->status)
                                <small class="text-success mr-1">
                                    <i class="fas fa-check"></i>
                                </small>
                                @else
                                <small class="text-danger mr-1">
                                    <i class="fas fa-times"></i>
                                </small>
                                @endif
                            </td>
                            <td>
                                @if($d->certificate)
                                <small class="text-success mr-1">
                                    <i class="fas fa-check"></i>
                                </small>
                                @else
                                <small class="text-danger mr-1">
                                    <i class="fas fa-times"></i>
                                </small>
                                @endif
                            </td>
                            <td>
                                <button wire:click="editOrganisasi({{ $d->id }})" data-toggle="modal" data-target="#certificateModal" title="Upload Sertifikat" class="btn btn-xs btn-secondary icon-button">
                                    <i class="fas fa-file"></i>
                                </button>
                                <button wire:click="approve({{ $d->id }}, {{ $d->status }})" title="Approve Pengajuan" class="btn btn-xs @if(!$d->status) btn-success @else btn-warning @endif icon-button"
                                    onclick="confirm('Are you sure to @if(!$d->status) approve @else reject @endif?') || event.stopImmediatePropagation()">
                                    <i class="fas @if(!$d->status) fa-check @else fa-times @endif"></i>
                                </button>
                                <button wire:click="editOrganisasi({{ $d->id }})" title="Buka Detail" data-toggle="modal" data-target="#organisasiModal" class="btn btn-xs btn-primary icon-button">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" title="Hapus Data" class="btn btn-xs btn-danger icon-button" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="4">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

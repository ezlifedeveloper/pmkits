<div class="row">
    <div class="col-lg-9">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data User</h3>
                <button class="btn btn-xs btn-light" wire:click="createUser()" style="float: right; margin: 0px 0px 10px 10px">Create User</button>
                <button wire:click="openImport()" class="btn btn-xs btn-primary" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-download pr-1"></i> Import</button>
                <button wire:click="exportExcel()" class="btn btn-xs btn-success" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-upload pr-1"></i> Export</button>
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:250px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Access</th>
                            <th max-width="150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->username}}</td>
                            <td>{{$d->email}}</td>
                            <td>{{$d->roleUser->name}}</td>
                            <td>{{$d->status ? 'Active' : 'Inactive'}}</td>
                            <td>{{$d->access}}</td>
                            <td>
                                <button wire:click="openUser({{ $d->id }})" class="btn btn-xs btn-info" title="Edit User" style="width:30px; height:30px; margin: 1px">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="resetPassword({{ $d->id }})" class="btn btn-xs btn-warning" title="Reset Password" style="width:30px; height:30px; margin: 1px" onclick="confirm('Are you sure to reset password?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-key"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" class="btn btn-xs btn-danger" title="Hapus User" style="width:30px; height:30px; margin: 1px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="5">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">

    @if($showUser)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"></h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <form method="post" wire:submit.prevent="storeUser()">
            <div class="card-body">
                <div class="form-group col-12">
                    <img src="{{$qrCode}}" style="width: 100%"/>
                </div>
                <div class="form-group col-12">
                    <label for="input_name">Name</label>
                    <input type="text" wire:model="input_name" id="input_name" class="form-control @error('input_name') is-invalid @enderror" required>
                    @error('input_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_username">Username</label>
                    <input type="text" wire:model="input_username" id="input_username" class="form-control @error('input_username') is-invalid @enderror" required>
                    @error('input_username') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_email">Email</label>
                    <input type="email" wire:model="input_email" id="input_email" class="form-control @error('input_email') is-invalid @enderror">
                    @error('input_email') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_password">Password</label>
                    <input type="password" wire:model="input_password" id="input_password" class="form-control @error('input_password') is-invalid @enderror">
                    @error('input_password') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_role">Role</label>
                    <select wire:model="input_role" class="form-control select2 @error('input_role') is-invalid @enderror" required>
                        @foreach ($type as $d)
                            <option value="{{$d->id}}">{{$d->name}}</option>
                        @endforeach
                    </select>
                    @error('input_role') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_access">Role</label>
                    <div>
                        <h6><input type="checkbox" wire:model="input_access" value="Bendahara"  multiple>Bendahara</h6>
                        <h6><input type="checkbox" wire:model="input_access" value="Dope"  multiple>Doa Pemerhati</h6>
                    </div>
                    @error('input_access') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
    @elseif($showActivity)
    @endif

    @if($showImport)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"> Import</h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <form method="post" wire:submit.prevent="storeImport()">
            <div class="card-body">
                <div class="form-group col-12">
                    <img src="{{$qrCode}}" style="width: 100%"/>
                </div>
                <div class="form-group col-12">
                    <label for="importName">Import Data</label>
                    <input type="file" wire:model="importName" id="importName"  @error('importName') is-invalid @enderror" required>
                    @error('importName') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    <br>
                    <small>Note <b class="text-danger">*</b> : Type file harus xlsx, xls, atau csv</small>
                </div>

            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-success">Import</button>
            </div>
        </form>
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>
    @endif
    </div>
</div>

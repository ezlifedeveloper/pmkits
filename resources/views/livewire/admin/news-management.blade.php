<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data Berita / Warta</h3>
                <button class="btn btn-xs btn-light" wire:click="createNews()" style="float: right; margin: 0px 0px 10px 10px">Create News</button>
                {{-- <button wire:click="openImport()" class="btn btn-xs btn-primary" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-download pr-1"></i> Import</button> --}}
                {{-- <button wire:click="exportExcel()" class="btn btn-xs btn-success" style="float: right; margin: 0px 0px 10px 10px"><i class="fas fa-upload pr-1"></i> Export</button> --}}
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>Judul</th>
                            <th>Link</th>
                            <th>Deskripsi</th>
                            <th>File</th>
                            <th>Dilihat</th>
                            <th width="120px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->title}}</td>
                            <td><a target="_blank" href="{{env('APP_URL').'/news/'.$d->slug}}">Event</a></td>
                            <td>{{$d->excerpt}}</td>
                            <td>
                                @foreach ($d->file as $f)
                                <p style="margin-bottom: 0px">
                                    <a href="{{$f->file}}" target="_blank">{{$f->name}}</a>
                                    <a wire:click="deleteFile({{$f->id}})" style="color: red"
                                        wire:onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></a>
                                </p>
                                @endforeach
                            </td>
                            <td>
                                <small class="text-secondary mr-1">
                                    <i class="fas fa-eye"></i>
                                    {{$d->read ?? 0}}
                                </small>
                            </td>
                            <td>
                                <button wire:click="createFile({{ $d->id }})" title="Upload File" class="btn btn-xs btn-secondary icon-button">
                                    <i class="fas fa-file"></i>
                                </button>
                                <button wire:click="openNews({{ $d->id }})" title="Edit Berita" class="btn btn-xs btn-info icon-button">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="openDetail({{ $d->id }})" title="Buka Detail Berita" class="btn btn-xs btn-primary icon-button">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" title="Hapus Berita" class="btn btn-xs btn-danger icon-button" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="4">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
    @if($showNews)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"></h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <form method="post" wire:submit.prevent="storeNews()">
            <div class="card-body">
                <div class="form-group col-12">
                    <label for="input_title">Judul Berita</label>
                    <input type="text" wire:model="input_title" id="input_title" class="form-control @error('input_title') is-invalid @enderror" required>
                    @error('input_title') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_description">Deskripsi</label>
                    <textarea wire:model="input_description" id="input_description" class="form-control @error('input_description') is-invalid @enderror" rows="5" required></textarea>
                    @error('input_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_url">Link Terkait (jika ada)</label>
                    <input type="text" wire:model="input_url" id="input_url" class="form-control @error('input_url') is-invalid @enderror">
                    @error('input_url') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_tag">Tag (Separated by , )</label>
                    <input type="text" wire:model="input_tag" id="input_tag" class="form-control @error('input_tag') is-invalid @enderror">
                    @error('input_tag') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="cover">Cover Image</label>
                    <input type="file" wire:model="cover" class="form-control">
                    @error('cover') <span class="error">{{ $message }}</span> @enderror
                    @if ($cover)
                    Photo Preview:
                    <img src="{{ !is_string($cover) ? $cover->temporaryUrl() : $cover }}" width="100%">
                    @endif
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
    @elseif($showDetail)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"></h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <div class="card-body">
            <div class="row" style="margin-top: 20px">
                <div class="col-sm-6">
                    <img src="{{$news->cover}}" style="width: 100%"/>
                </div>
                <div class="col-sm-6">
                    <b>Judul Berita</b>
                    <p style="margin-bottom: 5px;">
                        {{$news->title}}
                    </p>
                    <b>Deskripsi</b>
                    <p class="text-description">{{ $news->description }}</p>
                </div>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <img src="{{$news->qr}}" style="width: 100%"/>
                </div>
            </div>
        </div>
    </div>
    @elseif($showFile)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"></h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <form method="post" wire:submit.prevent="uploadFile({{$input_id}})">
            <div class="card-body">
                <div class="form-group col-12">
                    <label for="input_file">Nama File</label>
                    <input type="text" wire:model="input_file" id="input_file" class="form-control @error('input_file') is-invalid @enderror" required>
                    @error('input_file') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label for="input_photo">File</label>
                    <input type="file" wire:model="input_photo" class="form-control">
                    @error('input_photo') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
    @endif
    </div>
</div>

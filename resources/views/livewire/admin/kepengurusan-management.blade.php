<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data Kepengurusan</h3>
                <button class="btn btn-xs btn-light" wire:click="createData()" style="float: right; margin-left: 10px">Create Kepengurusan</button>
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>Tahun Kepengurusan</th>
                            <th>Status</th>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th width="150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->name}}</td>
                            <td>
                                @if($d->status)
                                <small class="text-success mr-1">
                                    <i class="fas fa-check"></i>
                                </small>
                                @else
                                <small class="text-warning mr-1">
                                    <i class="fas fa-minus"></i>
                                </small>
                                @endif
                            </td>
                            <td>{{$d->start}}</td>
                            <td>{{$d->end}}</td>
                            <td>
                                @if(!$d->status)
                                <button wire:click="setActive({{ $d->id }})" class="btn btn-xs btn-success icon-button" title="Set Active">
                                    <i class="fas fa-check"></i>
                                </button>
                                @endif
                                <button wire:click="openData({{ $d->id }})" class="btn btn-xs btn-info icon-button" title="Edit Data">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" class="btn btn-xs btn-danger icon-button" title="Delete data" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="4">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
    @if($showData)
    <div class="card">
        <div class="card-header bg-gradient-gray">
            <h3 class="card-title"></h3>
            <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
        </div>
        <form method="post" wire:submit.prevent="storeData()">
            <div class="card-body">
                <div class="form-group col-12">
                    <label for="input_name">Tahun Kepengurusan (ex: 2017/2018)</label>
                    <input type="text" wire:model="input_name" id="input_name" class="form-control @error('input_name') is-invalid @enderror" required>
                    @error('input_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label class="font-weight-bold">Tanggal Mulai Kepengurusan</label>
                    <input type="date" wire:model="input_start" class="form-control @error('input_start') is-invalid @enderror">
                    @error('input_start') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="form-group col-12">
                    <label class="font-weight-bold">Tanggal Selesai Kepengurusan</label>
                    <input type="date" wire:model="input_end" class="form-control @error('input_end') is-invalid @enderror">
                    @error('input_end') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
    @endif
    </div>
</div>

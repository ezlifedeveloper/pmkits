<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Data Prayer Request</h3>
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>Timestamp</th>
                            <th>Submitted By</th>
                            <th>Request</th>
                            <th>Report</th>
                            <th>Status</th>
                            <th width="150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->created_at}}</td>
                            <td>{{$d->visible ? $d->submitter->name : 'hidden'}}</td>
                            <td>{{$d->prayer}}</td>
                            <td>{{$d->report}}</td>
                            <td>
                                @if($d->prayed)
                                <small class="text-success mr-1">
                                    <i class="fas fa-check"></i>
                                </small>
                                @else
                                <small class="text-warning mr-1">
                                    <i class="fas fa-minus"></i>
                                </small>
                                @endif
                            </td>
                            <td>
                                @if(!$d->prayed)
                                <button wire:click="setActive({{ $d->id }})" class="btn btn-xs btn-success icon-button" title="Set Active">
                                    <i class="fas fa-check"></i>
                                </button>
                                @endif
                                <button wire:click="openPrayReq({{ $d->id }})" class="btn btn-xs btn-info icon-button" title="Edit Prayer Request">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="delete({{ $d->id }})" class="btn btn-xs btn-danger icon-button" title="Delete data" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="7">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
    @if($showPrayReq)
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title"></h3>
                <button class="btn btn-xs btn-light" wire:click="closeAll()" style="float: right">Close</button>
            </div>
            <form method="post" wire:submit.prevent="storePray()">
                <div class="card-body">
                    <div class="form-group col-12">
                        <label for="input_prayer">Prayer</label>
                        <input type="text" wire:model="input_prayer" id="input_prayer" class="form-control @error('input_prayer') is-invalid @enderror" required>
                        @error('input_prayer') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group col-12">
                        <label for="input_report">Report</label>
                        <textarea wire:model="input_report" id="input_report" class="form-control @error('input_report') is-invalid @enderror" rows="5" required></textarea>
                        @error('input_report') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
    @endif
</div>

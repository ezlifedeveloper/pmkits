<div class="card">
    <div class="card-header bg-gradient-gray">
        <h3 class="card-title">Dokumentasi</h3>
    </div>
    <div class="card-body row">
        @if(!$isOpen)
            @foreach ($gallery as $d)
                <div class="col-lg-4 col-sm-6 col-xs-12" style="margin-bottom: 10px">
                    <img src="{{$d->file}}" style="max-width:100%">
                </div>
            @endforeach
        @else
        <form method="post" wire:submit.prevent="storeImage()">
            <div class="form-group col-12">
                <label for="image">Add Image</label>
                <input type="file" wire:model="image" class="form-control">
                @error('image') <span class="error">{{ $message }}</span> @enderror
                @if ($image)
                Photo Preview:
                <img src="{{ !is_string($image) ? $image->temporaryUrl() : $image }}" width="100%">
                @endif
            </div>
        </form>
        @endif
    </div>

    <div class="card-footer text-right">
        @if($isOpen)
        <button type="button" wire:click="storeImage({{$eventid}})" class="btn btn-success">Save</button>
        @else
        <button type="button" wire:click="addImage({{$eventid}})" class="btn btn-success">Add Image</button>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header bg-gradient-gray">
                <h3 class="card-title">Konseling</h3>
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." style="float: right; width:200px;">
            </div>
            <div class="card-body p-0 table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>User</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Dibuat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $d)
                        <tr>
                            <td>{{ 10*($data->currentPage()-1)+$loop->iteration}}</td>
                            <td>{{$d->user}}</td>
                            <td>
                                @if($d->status)
                                <small class="text-success mr-1">
                                    <i class="fas fa-check"></i>
                                </small>
                                @else
                                <small class="text-warning mr-1">
                                    <i class="fas fa-minus"></i>
                                </small>
                                @endif
                            </td>
                            <td>{{$d->type}}</td>
                            <td>{{$d->create_at}}</td>
                        </tr>
                        @empty
                        <tr><td colspan="4">No Data Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            {{-- <div class="card-footer">
                @include('admin.layout.tablecountinfo')
                <div class="text-xs" style="float: right">
                @if($data->hasPages())
                    {{ $data->links() }}
                @endif
                </div>
            </div> --}}
        </div>
    </div>
</div>

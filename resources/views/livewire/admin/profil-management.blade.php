<div class="row">
    <div class="col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                @if(!$isEdit) <button class="btn btn-xs btn-light" wire:click="editData()">Edit Data</button>
                @else <button class="btn btn-xs btn-success" wire:click="saveData()">Simpan Data</button>
                @endif
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img src="{{asset('PMK.png')}}" style="max-width: 100%" alt="User profile picture">
                </div>
                <hr>

                <h3 class="profile-username text-center">PMK ITS</h3>
                <hr>

                <strong><i class="fas fa-sticky-note mr-1"></i>Deskripsi</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <textarea wire:model="input_detail" id="input_detail" class="form-control
                        @error('input_detail') is-invalid @enderror" rows="5" required></textarea>
                    @error('input_detail') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    {{ strlen($data->detail)>1 ? $data->detail : "-" }}
                    @endif
                </p>

                <strong><i class="fa fa-eye mr-1"></i>Visi</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <textarea wire:model="input_vision" id="input_vision" class="form-control
                        @error('input_vision') is-invalid @enderror" rows="5" required></textarea>
                    @error('input_vision') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    {{ strlen($data->vision)>1 ? $data->vision : "-" }}
                    @endif
                </p>

                <strong><i class="fa fa-target mr-1"></i>Misi</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <textarea wire:model="input_mission" id="input_mission" class="form-control
                        @error('input_mission') is-invalid @enderror" rows="5" required></textarea>
                    @error('input_mission') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    {{ strlen($data->mission)>1 ? $data->mission : "-" }}
                    @endif
                </p>

                <strong><i class="fas fa-building mr-1"></i>Alamat</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <textarea wire:model="input_address" id="input_address" class="form-control
                        @error('input_address') is-invalid @enderror" rows="5" required></textarea>
                    @error('input_address') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    {{ strlen($data->address)>1 ? $data->address : "-" }}
                    @endif
                </p>

                <strong><i class="fas fa-phone mr-1"></i>Phone</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_phone" id="input_phone" class="form-control @error('input_phone') is-invalid @enderror" required>
                    @error('input_phone') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    {{ strlen($data->phone)>1 ? $data->phone : "-" }}
                    @endif
                </p>

                <strong><i class="fas fa-email mr-1"></i>Email</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_email" id="input_email" class="form-control @error('input_email') is-invalid @enderror" required>
                    @error('input_email') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    {{ strlen($data->email)>1 ? $data->email : "-" }}
                    @endif
                </p>
                <hr>

                <strong><i class="fab fa-instagram mr-1"></i>Instagram</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_instagram" id="input_instagram" class="form-control @error('input_instagram') is-invalid @enderror" required>
                    @error('input_instagram') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    <a href="{{$data->instagram}}" target="_blank"> {{ $data->instagram }} </a>
                    @endif
                </p>

                <strong><i class="fab fa-line mr-1"></i>Line</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_line" id="input_line" class="form-control @error('input_line') is-invalid @enderror" required>
                    @error('input_line') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    <a href="{{$data->line}}" target="_blank"> {{ $data->line }} </a>
                    @endif
                </p>

                <strong><i class="fab fa-twitter mr-1"></i>Twitter</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_twitter" id="input_twitter" class="form-control @error('input_twitter') is-invalid @enderror" required>
                    @error('input_twitter') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    <a href="{{$data->twitter}}" target="_blank"> {{ $data->twitter }} </a>
                    @endif
                </p>

                <strong><i class="fab fa-facebook mr-1"></i>Facebook</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_facebook" id="input_facebook" class="form-control @error('input_facebook') is-invalid @enderror" required>
                    @error('input_facebook') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    <a href="{{$data->facebook}}" target="_blank"> {{ $data->facebook }} </a>
                    @endif
                </p>

                <strong><i class="fab fa-youtube mr-1"></i>Youtube</strong>
                <p class="text-muted">
                    @if($isEdit)
                    <input type="text" wire:model="input_youtube" id="input_youtube" class="form-control @error('input_youtube') is-invalid @enderror" required>
                    @error('input_youtube') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    @else
                    <a href="{{$data->youtube}}" target="_blank"> {{ $data->youtube }} </a>
                    @endif
                </p>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-primary">
            <div class="card-header">

            </div>
            <div class="card-body">
                <div class="form-group col-12">
                    <label for="homecover">Homescreen Background</label>
                    @if($isEdit)
                    <input type="file" wire:model="homecover" class="form-control" accept="image/png">
                    @error('homecover') <span class="error">{{ $message }}</span> @enderror
                    @else
                    <img src="{{asset('file/profilepmk/profile-homecover.png')}}" width="100%">
                    @endif
                    @if ($homecover)
                    Photo Preview:
                    <img src="{{ !is_string($homecover) ? $homecover->temporaryUrl() : $homecover }}" width="100%">
                    @endif
                </div>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">

            </div>
            <div class="card-body">
                <div class="form-group col-12">
                    <label for="banner">Banner Image</label>
                    @if($isEdit)
                    <input type="file" wire:model="banner" class="form-control" accept="image/png">
                    @error('banner') <span class="error">{{ $message }}</span> @enderror
                    @else
                    <img src="{{asset('file/profilepmk/profile-banner.png')}}" width="100%">
                    @endif
                    @if ($banner)
                    Photo Preview:
                    <img src="{{ !is_string($banner) ? $banner->temporaryUrl() : $banner }}" width="100%">
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

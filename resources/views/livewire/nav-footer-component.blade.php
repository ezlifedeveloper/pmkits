<div class="row" style="margin-top: 20px">
    <div class="col-xs-6 text-left">
        @if($state == 'detail' && $nextId)
        <a href={{route($nextRoute, $nextId)}}>
            <h5 style="margin-bottom:0px"><i class="fa fa-chevron-left" style="margin-right: 10px;"></i>{{$nextText}}</h5>
            <p style="margin: 5px 0px">{{$nextTitle}}</p>
        </a>
        @elseif($state == 'list')
        <a href={{route($nextRoute)}}>
            <h5 style="margin-bottom:0px"><i class="fa fa-chevron-left" style="margin-right: 10px;"></i>{{$nextText}}</h5>
            <p style="margin: 5px 0px">{{$nextTitle}}</p>
        </a>
        @endif
        {{-- <img src="{{asset('pmknatal.JPG')}}" width="200px" height="150px" style="max-width:100%; margin-right: 10px"> --}}
    </div>
    <div class="col-xs-6 text-right">
        @if($state == 'detail' && $prevId)
        <a href={{route($prevRoute, $prevId)}}>
            <h5 style="margin-bottom:0px">{{$prevText}}<i class="fa fa-chevron-right" style="margin-left: 10px;"></i></h5>
            <p style="margin: 5px 0px">{{$prevTitle}}</p>
        </a>
        @elseif($state == 'list')
        <a href={{route($prevRoute)}}>
            <h5 style="margin-bottom:0px">{{$prevText}}<i class="fa fa-chevron-right" style="margin-left: 10px;"></i></h5>
            <p style="margin: 5px 0px">{{$prevTitle}}</p>
        </a>
        @endif
        {{-- <img src="{{asset('bgheader.jpeg')}}" width="200px" height="150px" style="max-width:100%; margin-left: 10px"> --}}
    </div>
</div>

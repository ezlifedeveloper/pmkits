<div class="row">
    @foreach ($gallery as $d)
    <div class="col-sm-4 no-pad">
        <div class="portfolio-item">
            <a class="swipebox" href="{{$d->file}}" title="Photo Caption">
                <img src="{{$d->file}}" alt="{{$d->parent->name}}" style="height: 450px">
                <div class="portfolio-overlay">
                    <div class="caption">
                        <h5>{{$d->parent->name}}</h5><span>{{'Uploaded '.$d->created_at}}</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    @endforeach
</div>

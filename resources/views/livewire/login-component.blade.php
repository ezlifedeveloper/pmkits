<div>
    <!-- Login-->
    <img src="{{asset('PMK.png')}}" width="250px" style="margin-bottom: 20px"/>
    <h2>Sign in</h2>
    <div class="container">
        <div class="row wow fadeIn">
            <div class="col-md-4 col-md-offset-4">
                <form class="form-signin" wire:submit.prevent="login()">
                    <div class="form-group">
                        <label class="sr-only" for="inputEmail">Username</label>
                        <input class="form-control input-lg" wire:model="username" id="inputEmail" type="text" name="username" placeholder="Username" required autofocus="">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="inputPassword">Password</label>
                        <input class="form-control input-lg" wire:model="password" id="inputPassword" type="password" name="password" placeholder="Password" required>
                    </div>

                    <button class="btn btn-lg btn-dark btn-block" style="margin:0px" type="submit">
                        <div wire:loading wire:target="login">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Logging In....
                        </div>
                        <div wire:loading.remove wire:target="login">Log In</div>
                    </button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3"></div>
        </div>
    </div>
</div>

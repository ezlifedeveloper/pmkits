<div>
    <ul class="list-inline no-pad">
        <li><a target="_blank" href="{{$data->twitter}}"><i class="fab fa-twitter fa-fw fa-lg"></i></a></li>
        <li><a target="_blank" href="{{$data->facebook}}"><i class="fab fa-facebook-f la-fw fa-lg"></i></a></li>
        <li><a target="_blank" href="{{$data->youtube}}"><i class="fab fa-youtube fa-fw fa-lg"></i></a></li>
        <li><a target="_blank" href="{{$data->instagram}}"><i class="fab fa-instagram fa-fw fa-lg"></i></a></li>
        <li><a target="_blank" href="{{$data->line}}"><i class="fab fa-line fa-fw fa-lg"></i></a></li>
    </ul>
</div>

<div>
    <section class="bg-gray" id="blog">
        <div class="container">
            <div class="row" style="padding-bottom: 0px; margin-bottom: 0px; padding-top:-20px">
                <div class="col-sm-4 col-sm-offset-4" style="text-align: center">
                    <form class="form-inline subscribe-form" action="#" style="margin-bottom: 15px" method="post">
                        <div class="input-group input-group-lg" style="width: 100%">
                            <input class="form-control" wire:model='searchData' type="search" name="search" placeholder="Search..."><span class="input-group-btn">
                            <button class="btn btn-dark" type="submit" name="search"><i class="fa fa-search fa-lg"></i></button></span>
                        </div>
                    </form>
                    @include('web.layout.tablecountinfo')
                </div>
            </div>
            <div class="row grid-pad" style="padding-top: 0px;">
                @foreach ($lists as $d)
                <div class="col-sm-4" style="height: 480px">
                    <a href="{{route('renungan.detail', $d->slug)}}">
                        <img class="img-responsive center" height="250px" src="{{$d->cover}}" alt="">
                        <h4>
                            {{$d->title}}<br/>
                            <div class="row" style="padding-top: 0">
                                <small class="col-lg-6">{{$d->verse}}</small>
                                <small class="col-lg-6 text-right">{{$d->date->format('d M Y')}}</small>
                            </div>
                        </h4>
                    </a>
                    <p style="text-align: justify">{{$d->excerpt}}</p>
                </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    @if($lists->hasPages())
                        {{ $lists->links() }}
                    @endif
                </div>
            </div>

            @livewire('nav-footer-component', ['menu' => $menu, 'state' => 'list'])
        </div>
    </section>
</div>

<div>
    <section class="bg-gray" id="blog">
        <div class="container">
            <div class="row" style="padding-bottom: 0px; margin-bottom: 0px; padding-top:-20px">
                <div class="col-sm-4 col-sm-offset-4" style="text-align: center">
                    <form class="form-inline subscribe-form" action="#" style="margin-bottom: 15px" method="post">
                        <div class="input-group input-group-lg" style="width: 100%">
                            <input class="form-control" wire:model='searchData' type="search" name="search" placeholder="Search..."><span class="input-group-btn">
                            <button class="btn btn-dark" type="submit" name="search"><i class="fa fa-search fa-lg"></i></button></span>
                        </div>
                    </form>
                    @include('web.layout.tablecountinfo')
                </div>
            </div>
            @foreach ($lists as $d)
            @if($loop->iteration%3==0)<div class="row grid-pad" style="padding-top: 0px">@endif
                <div class="col-sm-4">
                    @if($d->report)
                    <a href="{{route('event.finished', $d->slug)}}">
                    @else
                    <a href="{{route('event.detail', $d->slug)}}">
                    @endif
                        <img class="img-responsive center" height="250px" style="margin-bottom:10px" src="{{$d->cover}}" alt="{{$d->name}}">
                        <h4>
                            {{$d->name}}<br/>
                            <small>{{$d->start->diffForHumans()}}</small>
                        </h4>
                    </a>
                    <p style="text-align: justify">{{$d->excerpt}}</p>
                </div>
            @if($loop->iteration%3==0)</div>@endif
            @endforeach
            <div class="row">
                <div class="col-lg-12 text-center">
                    @if($lists->hasPages())
                        {{ $lists->links() }}
                    @endif
                </div>
            </div>

            @livewire('nav-footer-component', ['menu' => $menu, 'state' => 'list'])
        </div>
    </section>
</div>

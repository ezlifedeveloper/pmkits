<div class="card">
    <div class="card-header bg-gradient-gray">
        <h3 class="card-title">Peserta</h3>
    </div>
    <div class="card-body p-0 table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th>Nama</th>
                    <th>NRP / Asal</th>
                    <th>Bukti Trf</th>
                </tr>
            </thead>
            <tbody>
                @forelse($presence as $d)
                <tr>
                    <td>{{ 10*($presence->currentPage()-1)+$loop->iteration}}</td>
                    <td>{{ $d->user ? $d->participant->name : $d->name}}</td>
                    <td>{{ $d->user ? $d->participant->username : $d->from}}</td>
                    <td>@if($d->file)<a href="{{$d->file}}" target="_blank">File</a>@endif</td>
                </tr>
                @empty
                <tr><td colspan="3">No Data Found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        Showing {{($presence->currentPage()-1)* $presence->perPage()+($presence->total() ? 1:0)}} to {{($presence->currentPage()-1)*$presence->perPage()+count($presence)}}  of  {{$presence->total()}}  Results
        <div class="text-xs" style="float: right">
        @if($presence->hasPages())
            {{ $presence->links()}}
        @endif
        </div>
    </div>
</div>

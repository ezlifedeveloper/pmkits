<div>
    <div wire:ignore.self class="modal fade" id="passwordModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="passwordModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" wire:submit.prevent="updatePassword()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="passwordModalLabel">Ubah Password</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_old_password">Password Lama</label>
                            <input type="password" wire:model="input_old_password" id="input_old_password" class="form-control @error('input_old_password') is-invalid @enderror" required>
                            @error('input_old_password') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_password">Password Baru</label>
                            <input type="password" wire:model="input_password" id="input_password" class="form-control @error('input_password') is-invalid @enderror" required>
                            @error('input_password') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_repassword">Ulang Password Baru</label>
                            <input type="password" wire:model="input_repassword" id="input_repassword" class="form-control @error('input_repassword') is-invalid @enderror" required>
                            @error('input_repassword') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="closeAll()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" wire:click.prevent="updatePassword()" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="profileModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="profileModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" wire:submit.prevent="storeProfile()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profileModalLabel">Ubah Profil</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_name">Name</label>
                            <input type="text" wire:model="input_name" id="input_name" class="form-control @error('input_name') is-invalid @enderror" required>
                            @error('input_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_username">Username</label>
                            <input type="text" wire:model="input_username" id="input_username" class="form-control @error('input_username') is-invalid @enderror" required>
                            @error('input_username') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_email">Email</label>
                            <input type="email" wire:model="input_email" id="input_email" class="form-control @error('input_email') is-invalid @enderror">
                            @error('input_email') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_birth_place">Tempat Lahir</label>
                            <input type="text" wire:model="input_birth_place" id="input_birth_place" class="form-control @error('input_birth_place') is-invalid @enderror" required>
                            @error('input_birth_place') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_birth_date">Tanggal Lahir</label>
                            <input type="date" wire:model="input_birth_date" id="input_birth_date" class="form-control @error('input_birth_date') is-invalid @enderror" required>
                            @error('input_birth_date') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_address_origin">Alamat Asal</label>
                            <input type="text" wire:model="input_address_origin" id="input_address_origin" class="form-control @error('input_address_origin') is-invalid @enderror" required>
                            @error('input_address_origin') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_address">Alamat Surabaya</label>
                            <input type="text" wire:model="input_address" id="input_address" class="form-control @error('input_address') is-invalid @enderror" required>
                            @error('input_address') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_phone">Nomor Telepon</label>
                            <input type="text" wire:model="input_phone" id="input_phone" class="form-control @error('input_phone') is-invalid @enderror" required>
                            @error('input_phone') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_phone_emergency">Nomor Telepon Emergency</label>
                            <input type="text" wire:model="input_phone_emergency" id="input_phone_emergency" class="form-control @error('input_phone_emergency') is-invalid @enderror" required>
                            @error('input_phone_emergency') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_photo">Foto Profil</label>
                            <input type="file" wire:model="input_photo" class="form-control">
                            @error('input_photo') <span class="error">{{ $message }}</span> @enderror
                            @if ($input_photo)
                            Photo Preview:
                            <img src="{{ !is_string($input_photo) ? $input_photo->temporaryUrl() : $input_photo }}" width="100%">
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="closeAll()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" wire:click.prevent="storeProfile()" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="pendidikanModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="pendidikanModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" wire:submit.prevent="storePendidikan()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="pendidikanModalLabel">Ubah Riwayat Pendidikan ITS</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_nrp">NRP</label>
                            <input type="text" wire:model="input_nrp" id="input_nrp" class="form-control @error('input_nrp') is-invalid @enderror" required>
                            @error('input_nrp') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_start">Mulai Studi</label>
                            <input type="date" wire:model="input_start" id="input_start" class="form-control @error('input_start') is-invalid @enderror" required>
                            @error('input_start') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_end">Selesai Studi</label>
                            <input type="date" wire:model="input_end" id="input_end" class="form-control @error('input_end') is-invalid @enderror" required>
                            @error('input_end') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_degree">Jenjang (D1/D3/D4/S1/S2/S3)</label>
                            <input type="text" style="text-transform: uppercase" maxlength="2" wire:model="input_degree" id="input_degree" class="form-control @error('input_degree') is-invalid @enderror" required>
                            @error('input_degree') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_faculty">Fakultas</label>
                            <input type="text" wire:model="input_faculty" id="input_faculty" class="form-control @error('input_faculty') is-invalid @enderror" required>
                            @error('input_faculty') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_department">Departemen</label>
                            <input type="text" wire:model="input_department" id="input_department" class="form-control @error('input_department') is-invalid @enderror" required>
                            @error('input_department') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="closeAll()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" wire:click.prevent="storePendidikan()" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="organisasiModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="organisasiModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" wire:submit.prevent="storeOrganisasi()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="organisasiModalLabel">Rekam Riwayat Organisasi</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputKepengurusan">Kepengurusan</label>
                            <select wire:model="inputKepengurusan" class="form-control select2 @error('inputKepengurusan') is-invalid @enderror" required>
                                <option value="" selected>-- Pilih Kepengurusan --</option>
                                @foreach ($kepengurusan as $k)
                                <option value="{{$k->id}}">{{'PMK ITS '.$k->name}}</option>
                                @endforeach
                            </select>
                            @error('inputKepengurusan') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_start">Mulai</label>
                            <input type="date" wire:model="input_start" id="input_start" class="form-control @error('input_start') is-invalid @enderror" required>
                            @error('input_start') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_end">Selesai</label>
                            <input type="date" wire:model="input_end" id="input_end" class="form-control @error('input_end') is-invalid @enderror" required>
                            @error('input_end') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_position">Posisi</label>
                            <input type="text" wire:model="input_position" id="input_position" class="form-control @error('input_position') is-invalid @enderror" required>
                            @error('input_position') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group">
                            <label for="input_description">Deskripsi</label>
                            <input type="text" wire:model="input_description" id="input_description" class="form-control @error('input_description') is-invalid @enderror">
                            @error('input_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="closeAll()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" wire:click.prevent="storeOrganisasi()" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <section class="bg-white" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-12 text-center">
                    <p><img class="img-circle center-block img-responsive" src="{{ $data->photo ?? asset('ITS.jpeg') }}" alt="" style="object-fit:cover; width:250px; height: 250px"></p>
                    <h4 style="margin-bottom: 5px">{{ $data->name }}<br/><small>{{ $data->roleUser->name }}</small></h4>
                    <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#passwordModal" style="margin-bottom: 10px">Change Pasword</button>
                    <p><img src="{{ $data->qr }}" alt="" style="max-width: 100%"></p>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <h4 class="bg-gray2" style="padding: 10px; margin: 10px 0px 0px 0px">Biodata</h4>
                    <div class="row" style="padding: 10px 0px">
                        <div class="col-xs-8">
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">Nama</div>
                                <div class="col-xs-8">{{ $data->name }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">Username</div>
                                <div class="col-xs-8">{{ $data->username }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">Email</div>
                                <div class="col-xs-8">{{ $data->email }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">TTL</div>
                                <div class="col-xs-8">{{ ($data->birth_place ?? "-").', '.($data->birth_date ?? "-/-/-") }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">Alamat Asal</div>
                                <div class="col-xs-8">{{ $data->address_origin ?? "-" }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">Alamat Domisili</div>
                                <div class="col-xs-8">{{ $data->address ?? "-" }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">No Telp</div>
                                <div class="col-xs-8">{{ $data->phone ?? "-" }}</div>
                            </div>
                            <div class="row" style="padding: 0px 0px 5px 10px;">
                                <div class="col-xs-4">No Telp Urgent</div>
                                <div class="col-xs-8">{{ $data->phone_emergency ?? "-" }}</div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <button wire:click='setProfileVariable()' class="btn btn-xs btn-primary" data-toggle="modal" data-target="#profileModal" style="float:right; margin:5px">Edit Profile</button>
                            {{-- <button wire:click='deletePendidikan({{$d->id}})' class="btn btn-xs btn-danger" style="float:right; margin:5px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">Hapus Data</button> --}}
                        </div>
                    </div>

                    <h4 class="bg-gray2" style="padding: 10px; margin: 10px 0px 0px 0px">Riwayat Pendidikan</h4>
                    <div class="row" style="padding: 10px">
                        @forelse($data->pendidikan as $d)
                        <div class="col-xs-8">
                            <h5 style="margin-bottom: 0px">{{ $d->nrp }}</h5>
                            <small>{{ $d->start->format('d M Y').' - '.($d->graduate ? $d->graduate->format('d M Y') : 'now') }}</small>
                            <h4>{{$d->department.' - '.$d->faculty.' ('.$d->degree.')'}}</h4>
                        </div>
                        <div class="col-xs-4">
                            <button wire:click='editPendidikan({{$d->id}})' class="btn btn-xs btn-primary" data-toggle="modal" data-target="#pendidikanModal" style="float:right; margin:5px">Edit Data</button>
                            {{-- <button wire:click='deletePendidikan({{$d->id}})' class="btn btn-xs btn-danger" style="float:right; margin:5px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">Hapus Data</button> --}}
                        </div>
                        @empty
                        <div class="col-xs-12">Data Tidak Ditemukan</div>
                        @endforelse

                        {{-- <div class="col-xs-12" style="margin-top: 15px"><a data-toggle="modal" data-target="#pendidikanModal">Tambah Data Riwayat Pendidikan</a></div> --}}
                    </div>

                    <h4 class="bg-gray2" style="padding: 10px; margin: 10px 0px 0px 0px">Riwayat Organisasi</h4>
                    <div class="row" style="padding: 10px">
                        @forelse($data->organisasi as $d)
                        <div class="col-xs-8" style="margin-bottom: 15px">
                            <small>
                                {{ $d->start->format('d M Y').' - '.($d->finish ? $d->finish->format('d M Y') : 'now') }}
                            </small>
                            <h4 style="margin-bottom: 0px">{{$d->position.' | PMK ITS '.$d->parent->name}}</h4>
                            <p style="margin-bottom: 0px">{{ $d->description }}</p>
                            @if($d->status) <span class="badge" style="background-color: green">Disetujui</span>
                            @else <span class="badge" style="background-color: grey">Menunggu Persetujuan</span>
                            @endif
                            @if($d->certificate) <a href="{{$d->certificate}}" target="_blank"><span class="badge" style="background-color: blue">Download Sertifikat</span></a>
                            @else <span class="badge" style="background-color: grey">Sertifikat Belum Tersedia</span>
                            @endif
                        </div>
                        <div class="col-xs-4">
                            <button wire:click='editOrganisasi({{$d->id}})' class="btn btn-xs btn-primary" data-toggle="modal" data-target="#organisasiModal" style="float:right; margin:5px">Edit Data</button>
                            {{-- <button wire:click='deleteOrganisasi({{$d->id}})' class="btn btn-xs btn-danger" style="float:right; margin:5px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()">Hapus Data</button> --}}
                        </div>
                        @empty
                        <div class="col-xs-12">Data Tidak Ditemukan</div>
                        @endforelse

                        <div class="col-xs-12" style="margin-top: 15px"><a data-toggle="modal" data-target="#organisasiModal">Tambah Data Riwayat Organisasi</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

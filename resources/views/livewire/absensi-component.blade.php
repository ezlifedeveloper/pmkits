<div class="row" style="margin-bottom: 2vh; padding-top: 0px">
    <h2 class="bold">
        {{$data->name}}<br/>
        {{$data->title}}<br/>
    </h2>
    @if($isAbsenced)
        <h3 style="margin: 2vh 0vh">Absensi berhasil dilakukan</h3>
    @elseif($message)
    <h3 style="margin: 2vh 0vh">{{$message}}</h3>
    @else
        <form method="post" wire:submit.prevent="savePresence()">
            @if(!$user)
            <h3 style="margin: 2vh 0vh">Masukkan data diri Anda</h3>
            <div class="form-group col-12">
                <input type="text" wire:model="input_name" id="input_name" placeholder="Your Name" style="display:block; margin: 10px auto; width: 50%; text-align:center"
                    class="form-control @error('input_name') is-invalid @enderror" required>
                @error('input_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>
            <div class="form-group col-12">
                <input type="text" wire:model="input_from" id="input_from" placeholder="Institution / Work / Church" style="display:block; margin: 10px auto; width: 50%; text-align:center"
                    class="form-control @error('input_from') is-invalid @enderror" required>
                @error('input_from') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>
            @else
            <h3>{{$user->name}}</h3>
            @endif
            @if($data->transfer)
            <div class="form-group col-12">
                <label for="file">Bukti Transfer</label>
                <input type="file" wire:model="file" class="form-control" accept="image/*" style="display:block; margin: 10px auto; width: 50%; text-align:center" required>
                @error('file') <span class="error">{{ $message }}</span> @enderror
            </div>
            @endif
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    @endif
</div>

<?php

namespace Database\Seeders;

use App\Models\Renungan;
use Illuminate\Database\Seeder;

class RenungansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Renungan::create([
            'title' => 'gereja tua',
            'verse' => 'markus 10:11',
            'slug' => 'slug 123',
            'source' => 'kitabsuci',
            'description' => 'Logasjfnsanfnsa,nf,m.afnasnfnasnfsanmfnams,nf,mansfnsam,nfasnfas',
            'excerpt' => 'sadsadsa',
            'date' => '2017-05-01',
            'read' => 0,
            'kepengurusan' => 1,
            'created_by' => 1,


        ]);

        Renungan::factory(5)->create();
    }
}

<?php

namespace Database\Seeders;

use App\Models\PrayerRequest;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(KepengurusanSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(CounselingsSeeder::class);
        $this->call(RenungansSeeder::class);
        $this->call(PrayerReqSeeder::class);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Kepengurusan;
use Illuminate\Database\Seeder;

class KepengurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kepengurusan::truncate();
        Kepengurusan::create([
            'name' => '2017/2018',
            'start' => '2017-05-01',
            'end' => '2018-04-30',
            'created_by' => 1
        ]);
        Kepengurusan::create([
            'name' => '2018/2019',
            'start' => '2018-05-01',
            'end' => '2019-04-30',
            'created_by' => 1
        ]);
        Kepengurusan::create([
            'name' => '2019/2020',
            'start' => '2019-05-01',
            'end' => '2020-04-30',
            'created_by' => 1
        ]);
        Kepengurusan::create([
            'name' => '2020/2021',
            'start' => '2020-05-01',
            'end' => '2021-04-30',
            'created_by' => 1
        ]);
        Kepengurusan::create([
            'name' => '2021/2022',
            'start' => '2021-05-01',
            'end' => '2022-04-30',
            'status' => 1,
            'created_by' => 1
        ]);
    }
}

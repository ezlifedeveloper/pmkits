<?php

namespace Database\Seeders;

use App\Models\PrayerRequest;
use Illuminate\Database\Seeder;

class PrayerReqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PrayerRequest::truncate();
        PrayerRequest::create([
            'user' =>1,
            'prayer'=> 'Doa bapa kami',
        ]);
        PrayerRequest::create([
            'user' =>2,
            'prayer'=> 'Diberi kesembuhan',
        ]);
        PrayerRequest::create([
            'user' =>3,
            'prayer'=> 'Salam Maria',
        ]);
    }
}

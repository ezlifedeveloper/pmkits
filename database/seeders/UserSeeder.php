<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::create([
            'name' => 'Superuser',
            'username' => 'superuser',
            'email' => 'superuser',
            'role' => 1,
            'password' => bcrypt('su')
        ]);

        // User::create([
        //     'name' => 'Prasetyo Nugrohadi',
        //     'username' => 'prasetyon',
        //     'email' => 'prstyngrhd@gmail.com',
        //     'role' => 4,
        //     'password' => bcrypt('123'),
        //     'access' => 'Bendahara'
        // ]);

        // User::create([
        //     'name' => 'Nur Husodo',
        //     'username' => 'nurhusodo',
        //     'email' => 'nurhusodo@gmail.com',
        //     'role' => 3,
        //     'password' => bcrypt('123'),
        //     'access' => 'Dope'
        // ]);

        // User::factory(5)->create();
    }
}

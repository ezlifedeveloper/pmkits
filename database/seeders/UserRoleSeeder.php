<?php

namespace Database\Seeders;

use App\Models\UserType;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::truncate();
        UserType::create(['name' => 'Superuser']);
        UserType::create(['name' => 'Mahasiswa']);
        UserType::create(['name' => 'Dosen']);
        UserType::create(['name' => 'Alumni']);
    }
}

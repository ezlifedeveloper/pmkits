<?php

namespace Database\Seeders;

use App\Models\ProfilPMK;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProfilPMK::truncate();
        ProfilPMK::create([
            'vision' => 'Menjadi alat untuk memperkenalkan Kristus kepada mahasiswa Kristen ITS dan wadah bersekutu dalam membangun iman Kristen yang berintegritas kepada Tuhan dan sesama',
            'mission' => '',
            'detail' => 'PMK ITS adalah organisasi mahasiswa ITS yang bergerak dalam bidang kerohahian Kristen yang berada di bawah naungan Tim Pembina Kerohanian Kristen ITS',
            'address' => 'Gedung SCC lt 2, Jalan Raya ITS Campus ITS Sukolilo Surabaya Jawa Timur 60111, Keputih, Kec. Sukolilo, Kota SBY, Jawa Timur 60117',
            'instagram' => 'https://www.instagram.com/pmk_its/',
            'line' => 'https://line.me/ti/p/~@cox6942k',
            'phone' => '',
            'email' => '',
            'youtube' => 'https://www.youtube.com/channel/UCi-OPHvUK0ycxF7u_OXEWEA',
            'facebook' => 'https://www.facebook.com/PMK-ITS-2036199983269774/',
            'twitter' => 'https://twitter.com/pmk_its'
        ]);
    }
}

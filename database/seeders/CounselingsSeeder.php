<?php

namespace Database\Seeders;

use App\Models\Counseling;
use Illuminate\Database\Seeder;

class CounselingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Counseling::truncate();
        Counseling::create([
            'user' => 1,
            'status'=> 0,
            'type' => 4,
        ]);
        Counseling::create([
            'user' => 32,
            'status'=> 0,
            'type' => 2,
        ]);
        Counseling::create([
            'user' => 1321,
            'status'=> 0,
            'type' => 2,
        ]);
        Counseling::create([
            'user' => 11,
            'status'=> 0,
            'type' => 4,
        ]);
        Counseling::create([
            'user' => 13,
            'status'=> 0,
            'type' => 4,
        ]);
        Counseling::create([
            'user' => 14,
            'status'=> 0,
            'type' => 4,
        ]);
    }
}

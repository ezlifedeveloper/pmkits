<?php

namespace Database\Factories;

use App\Models\Renungan;
use Illuminate\Database\Eloquent\Factories\Factory;

class RenunganFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Renungan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(mt_rand(2,5)),
            'verse' => 'markus 10:11',
            'slug' => $this->faker->sentence(mt_rand(2,8)),
            'source' => 'kitabsuci',
            'description' => $this->faker->paragraph(1),
            'excerpt' => $this->faker->sentence(mt_rand(2,10)),
            'date' => $this->faker->date(),
            'read' => 0,
            'kepengurusan' => 1,
            'created_by' =>1,
        ];
    }
}

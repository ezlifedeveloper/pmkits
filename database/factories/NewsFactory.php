<?php

namespace Database\Factories;

use App\Models\News;
use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(mt_rand(2,8)),
            'slug' => $this->faker->slug(),
            'description' => $this->faker->paragraph(1),
            'excerpt' => $this->faker->sentence(mt_rand(2,10)),
            'read' => 0,
            'kepengurusan' => 1,
            'created_by' => 1,
        ];
    }
}

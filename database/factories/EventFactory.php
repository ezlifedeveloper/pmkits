<?php

namespace Database\Factories;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $date = Carbon::create(2021, 12, 01, 0, 0, 0);

        return [
            'name' => $this->faker->sentence(mt_rand(2,8)),
            'slug' => $this->faker->slug(),
            'description' => $this->faker->paragraph(1),
            'excerpt' => $this->faker->sentence(mt_rand(10,25)),
            'start' => $date->format('Y-m-d H:i:s'),
            'end'  => $date->addWeeks(rand(1, 52))->format('Y-m-d H:i:s'),
            'read' => 0,
            'kepengurusan' => 1,
            'created_by' =>1,
        ];
    }
}

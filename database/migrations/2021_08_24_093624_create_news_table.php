<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->string('qr')->nullable();
            $table->longText('description');
            $table->string('excerpt');
            $table->string('tag')->nullable();
            $table->string('cover')->nullable();
            $table->string('url')->nullable();
            $table->unsignedMediumInteger('read')->default(0);
            $table->foreignId('kepengurusan')->constraint('kepengurusans');
            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}

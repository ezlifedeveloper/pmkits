<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatOrganisasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_organisasis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user')->constraint('users')->nullable();
            $table->foreignId('kepengurusan')->constraint('kepengurusans');
            $table->date('start')->nullable();
            $table->date('finish')->nullable();
            $table->string('position');
            $table->string('description')->nullable();
            $table->string('certificate')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_organisasis');
    }
}

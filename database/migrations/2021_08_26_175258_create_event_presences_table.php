<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventPresencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_presences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event')->constraint('events');
            $table->foreignId('user')->constraint('users')->nullable();
            $table->string('name')->nullable();
            $table->string('from')->nullable();
            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_presences');
    }
}

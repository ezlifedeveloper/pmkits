<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('qr')->nullable();
            $table->string('access_link')->nullable();
            $table->string('absence_link')->nullable();
            $table->longText('description');
            $table->string('excerpt');
            $table->string('tag')->nullable();
            $table->timestamp('start')->useCurrent();
            $table->timestamp('end')->useCurrent();
            $table->timestamp('open')->nullable();
            $table->timestamp('close')->nullable();
            $table->string('cover')->nullable();
            $table->boolean('isFeatured')->default(false);
            $table->string('hdcover')->nullable();
            $table->unsignedMediumInteger('participant')->default(0);
            $table->string('location')->nullable();
            $table->string('speaker')->nullable();
            $table->string('title')->nullable();
            $table->string('verse')->nullable();
            $table->string('url')->nullable();
            $table->boolean('show')->default(true);
            $table->longText('report')->nullable();
            $table->unsignedMediumInteger('read')->default(0);
            $table->foreignId('kepengurusan')->constraint('kepengurusans');
            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

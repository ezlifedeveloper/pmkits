<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilPMKSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profil_p_m_k_s', function (Blueprint $table) {
            $table->id();
            $table->string('vision');
            $table->string('mission');
            $table->string('detail');
            $table->string('address');
            $table->string('instagram')->nullable();
            $table->string('line')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('youtube')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->boolean('status')->default(1);
            $table->string('homecover')->nullable();
            $table->string('pagebanner')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profil_p_m_k_s');
    }
}

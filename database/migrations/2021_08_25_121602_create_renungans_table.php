<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenungansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renungans', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('verse');
            $table->string('slug');
            $table->string('source');
            $table->string('qr')->nullable();
            $table->longText('description');
            $table->string('excerpt');
            $table->string('tag')->nullable();
            $table->string('cover')->nullable();
            $table->date('date');
            $table->unsignedMediumInteger('read')->default(0);
            $table->foreignId('kepengurusan')->constraint('kepengurusans');
            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renungans');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('birth_place', 90)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('address')->nullable();
            $table->string('address_origin')->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('phone_emergency', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('birth_place');
            $table->dropColumn('birth_date');
            $table->dropColumn('address');
            $table->dropColumn('address_origin');
            $table->dropColumn('phone');
            $table->dropColumn('phone_emergency');
        });
    }
}

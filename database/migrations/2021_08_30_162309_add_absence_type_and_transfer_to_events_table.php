<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAbsenceTypeAndTransferToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('absence_type')->default('Absensi');
            $table->boolean('transfer')->default(false);
        });

        Schema::table('event_presences', function (Blueprint $table) {
            $table->string('file')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('absence_type');
            $table->dropColumn('transfer');
        });

        Schema::table('event_presences', function (Blueprint $table) {
            $table->dropColumn('file');
        });
    }
}

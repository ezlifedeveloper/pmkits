<?php

namespace App\Http\Livewire;

use App\Models\Event;
use App\Models\EventPresence;
use Livewire\Component;
use Livewire\WithPagination;

class ParticipantComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $eventid;

    public function mount($eventid)
    {
        $this->eventid = $eventid;
    }

    public function render()
    {
        return view('livewire.participant-component', [
            'presence' => EventPresence::where('event', $this->eventid)->paginate(15, ['*'], 'presence'),
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Event',
            ]);
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Event;
use App\Models\EventImage;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class GalleryComponent extends Component
{
    use WithFileUploads;

    public function render()
    {
        return view('livewire.gallery-component', [
            'gallery' => EventImage::orderBy('created_at', 'desc')->limit(3)->get()
        ]);
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Renungan;
use Livewire\Component;

class RenunganListComponent extends Component
{
    public $searchData;
    public function render()
    {
        $search = $this->searchData;

        return view('livewire.renungan-list-component', [
            'lists' => Renungan::when($search, function ($searchQuery) use ($search) {
                $searchQuery->where(function ($searchQuery) use ($search) {
                    $searchQuery->orWhere('title', 'like', '%' . $search . '%')
                        ->orWhere('description', 'like', '%' . $search . '%')
                        ->orWhere('tag', 'like', '%' . $search . '%');
                });
            })->orderBy('date', 'desc')->paginate(9),
            'menu' => 'Renungan'
        ])->extends('web.app')
            ->layoutData([
                'title' => 'Renungan',
                'menu' => 'Renungan',
            ]);
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\PrayerRequest;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class PrayerRequestComponent extends Component
{
    public $input_prayer;

    protected $rules = [
        'input_prayer' => 'required|min:1',
    ];

    public function render()
    {
        return view('livewire.prayer-request-component', [
            'lists' => PrayerRequest::whereNull('report')->orderBy('created_at', 'desc')->get(),
            'lists2' => PrayerRequest::where('user',Auth::id())->get(),
        ])->extends('web.app')
            ->layoutData([
                'title' => 'Prayer Request',
                'menu' => 'Prayer',
            ]);
    }

    public function store()
    {
        $this->validate();

        if (Auth::check()) {
            $submit = PrayerRequest::create([
                'user' => Auth::id(),
                'prayer' => $this->input_prayer
            ]);
        } else {
            $submit = PrayerRequest::create([
                'prayer' => $this->input_prayer
            ]);
        }

        $this->reset(['input_prayer']);
        $this->alert('success', 'Prayer Request berhasil disimpan');
    }
}

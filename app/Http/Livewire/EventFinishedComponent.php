<?php

namespace App\Http\Livewire;

use App\Models\Event;
use App\Models\EventImage;
use Livewire\Component;

class EventFinishedComponent extends Component
{
    public $data, $gallery;
    public $mobile;

    public function mount($id, $mobile = 0)
    {
        $this->mobile = $mobile;
        $this->data = Event::where('slug', $id)->first();

        if (!$this->data) abort(404);
        $this->gallery = EventImage::where('event', $this->data->id)->limit(9)->get();
        $update = Event::where('slug', $id)->increment('read');
    }

    public function render()
    {
        return view('livewire.event-finished-component')
            ->extends('web.app')
            ->layoutData([
                'title' => $this->data->name,
                'menu' => 'Event',
                'data' => $this->data,
                'mobile' => $this->mobile,
            ]);
    }
}

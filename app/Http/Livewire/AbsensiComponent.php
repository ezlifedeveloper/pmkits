<?php

namespace App\Http\Livewire;

use App\Helpers\Helper;
use App\Models\Event;
use App\Models\EventPresence;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Livewire\Component;
use Livewire\WithFileUploads;

class AbsensiComponent extends Component
{
    use WithFileUploads;

    public $data;
    public $isAbsenced = false;
    public $detail;
    public $now;
    public $user;
    public $message;
    public $mobile;
    public $isAdmin;
    public $iduser;
    public $file;
    public $input_name, $input_from;

    public function mount($event, $username = null, $admin = 0, $mobile = 0, $iduser = 0)
    {
        $this->data = Event::where('slug', $event)->first();
        $this->mobile = $mobile;
        $this->iduser = $iduser;
        $this->isAdmin = $admin;
        if (!$this->data) abort(404);

        $this->user = User::where('username', $username)->first();
        $this->presence();
    }

    public function render()
    {
        return view('livewire.absensi-component')
            ->extends('web.app')
            ->layoutData([
                'title' => 'Presensi Kehadiran ' . $this->data->name,
                'menu' => 'Event',
                'mobile' => $this->mobile,
                'data' => $this->data,
            ]);
    }

    public function savePresence()
    {
        if ($this->user) {
            if ($this->data->transfer) {
                $ext = '.' . $this->file->getClientOriginalExtension();
                $this->file->storeAs('event', $this->data->slug . '-transfer-' . $this->user->username . $ext);

                EventPresence::create([
                    'event' => $this->data->id,
                    'user' => $this->user->id,
                    'file' => env('APP_URL') . '/file/event/' . $this->data->slug . '-transfer-' . $this->user->username . $ext,
                    'created_by' => Auth::id()
                ]);
            } else {
                EventPresence::create([
                    'event' => $this->data->id,
                    'user' => $this->user->id,
                    'created_by' => Auth::id()
                ]);
            }
        } else {
            if (!$this->data->transfer) {
                EventPresence::create([
                    'event' => $this->data->id,
                    'name' => $this->input_name,
                    'from' => $this->input_from,
                    'created_by' => Auth::id()
                ]);
            } else {
                $ext = '.' . $this->file->getClientOriginalExtension();
                $this->file->storeAs('event', $this->data->slug . '-transfer-' . Helper::cleanStr($this->input_name) . $ext);

                EventPresence::create([
                    'event' => $this->data->id,
                    'name' => $this->input_name,
                    'from' => $this->input_from,
                    'file' => env('APP_URL') . '/file/event/' . $this->data->slug . '-transfer-' . Helper::cleanStr($this->input_name) . $ext,
                    'created_by' => Auth::id()
                ]);
            }
        }

        $update = Event::where('id', $this->data->id)->increment('participant');
        $this->isAbsenced = true;
        if ($this->isAbsenced && $this->data->url) return Redirect::to($this->data->url);
    }

    public function presence()
    {
        if (Auth::check() && $this->user) {
            $checkAbsence = EventPresence::where('event', $this->data->id)->whereNotNull('user')->where('user', $this->user->id)->first();
            if ($checkAbsence) $this->isAbsenced = true;
            else if (!$this->data->transfer && Auth::user()->roleUser->name == 'Superuser') {
                EventPresence::create([
                    'event' => $this->data->id,
                    'user' => $this->user->id,
                    'created_by' => Auth::id()
                ]);
                $update = Event::where('id', $this->data->id)->increment('participant');
                $this->isAbsenced = true;
                if ($this->isAbsenced && $this->data->url) return Redirect::to($this->data->url);
            } else if (!$this->data->transfer && Auth::id() == $this->user->id) {
                $now = Carbon::now('Asia/Jakarta');
                if ($this->data->show && $this->data->open->lessThan($now) && $this->data->close->greaterThanOrEqualTo($now)) {
                    EventPresence::create([
                        'event' => $this->data->id,
                        'user' => $this->user->id,
                        'created_by' => Auth::id()
                    ]);
                    $update = Event::where('id', $this->data->id)->increment('participant');
                    $this->isAbsenced = true;
                } else if (!$this->data->show) {
                    $this->message = 'Hubungi panitia untuk melakukan absensi';
                } else if (!$this->data->open->lessThan($now)) {
                    $this->message = 'Waktu absensi belum dibuka';
                } else if (!$this->data->close->greaterThanOrEqualTo($now)) {
                    $this->message = 'Waktu absensi telah ditutup';
                } else {
                    $this->message = 'Absensi tidak dapat dilakukan';
                }
                if ($this->isAbsenced && $this->data->url) return Redirect::to($this->data->url);
            } else if (!$this->data->transfer) {
                $this->message = 'Absensi tidak dapat dilakukan. Data tidak sesuai';
            }
        } else if ($this->mobile && $this->user) {
            $checkAbsence = EventPresence::where('event', $this->data->id)->whereNotNull('user')->where('user', $this->user->id)->first();
            if ($checkAbsence) $this->isAbsenced = true;
            else if (!$this->data->transfer && $this->isAdmin) {
                EventPresence::create([
                    'event' => $this->data->id,
                    'user' => $this->user->id,
                    'created_by' => $this->iduser
                ]);
                $update = Event::where('id', $this->data->id)->increment('participant');
                $this->isAbsenced = true;
                if ($this->isAbsenced && $this->data->url) return Redirect::to($this->data->url);
            } else if (!$this->data->transfer && $this->iduser == $this->user->id) {
                $now = Carbon::now('Asia/Jakarta');
                if ($this->data->show && $this->data->open->lessThan($now) && $this->data->close->greaterThanOrEqualTo($now)) {
                    EventPresence::create([
                        'event' => $this->data->id,
                        'user' => $this->user->id,
                        'created_by' => $this->iduser
                    ]);
                    $update = Event::where('id', $this->data->id)->increment('participant');
                    $this->isAbsenced = true;
                } else if (!$this->data->show) {
                    $this->message = 'Hubungi panitia untuk melakukan absensi';
                } else if (!$this->data->open->lessThan($now)) {
                    $this->message = 'Waktu absensi belum dibuka';
                } else if (!$this->data->close->greaterThanOrEqualTo($now)) {
                    $this->message = 'Waktu absensi telah ditutup';
                } else if (!$this->data->transfer) {
                    $this->message = 'Absensi tidak dapat dilakukan';
                }
                if ($this->isAbsenced && $this->data->url) return Redirect::to($this->data->url);
            } else {
                $this->message = 'Absensi tidak dapat dilakukan. Data tidak sesuai';
            }
        } else {
        }
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\News;
use Livewire\Component;

class NewsListComponent extends Component
{
    public $searchData;
    public function render()
    {
        $search = $this->searchData;

        return view('livewire.news-list-component', [
            'lists' => News::when($search, function ($searchQuery) use ($search) {
                $searchQuery->where(function ($searchQuery) use ($search) {
                    $searchQuery->orWhere('title', 'like', '%' . $search . '%')
                        ->orWhere('description', 'like', '%' . $search . '%')
                        ->orWhere('tag', 'like', '%' . $search . '%');
                });
            })->orderBy('created_at', 'desc')->paginate(9),
            'menu' => 'News'
        ])->extends('web.app')
            ->layoutData([
                'title' => 'News',
                'menu' => 'News',
            ]);
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Event;
use Livewire\Component;

class EventListComponent extends Component
{
    public $searchData;

    public function render()
    {
        $search = $this->searchData;

        return view('livewire.event-list-component', [
            'lists' => Event::when($search, function ($searchQuery) use ($search) {
                $searchQuery->where(function ($searchQuery) use ($search) {
                    $searchQuery->where('name', 'like', '%' . $search . '%')
                        ->orWhere('title', 'like', '%' . $search . '%')
                        ->orWhere('description', 'like', '%' . $search . '%')
                        ->orWhere('tag', 'like', '%' . $search . '%');
                });
            })->orderBy('start', 'desc')->paginate(9),
            'menu' => 'Event'
        ])->extends('web.app')
            ->layoutData([
                'title' => 'Event',
                'menu' => 'Event',
            ]);
    }
}

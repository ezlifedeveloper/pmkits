<?php

namespace App\Http\Livewire;

use App\Models\Event;
use App\Models\News;
use App\Models\Renungan;
use Livewire\Component;

class NavFooterComponent extends Component
{
    public $menu, $state;
    public $nextRoute, $nextText, $nextTitle, $nextId;
    public $prevRoute, $prevText, $prevTitle, $prevId;

    public function mount($menu, $state, $id = 0)
    {
        $this->menu = $menu;
        $this->state = $state;

        if ($state == 'list') {
            if ($menu == 'Event') {
                $this->nextRoute = 'home';
                $this->nextText = 'Home';
                $this->nextTitle = 'Buka Halaman Home';
                $this->prevRoute = 'news.index';
                $this->prevText = 'News';
                $this->prevTitle = 'Buka Halaman News';
            } else if ($menu == 'News') {
                $this->nextRoute = 'event.index';
                $this->nextText = 'Event';
                $this->nextTitle = 'Buka Halaman Event';
                $this->prevRoute = 'renungan.index';
                $this->prevText = 'Renungan';
                $this->prevTitle = 'Buka Halaman Renungan';
            } else if ($menu == 'Renungan') {
                $this->nextRoute = 'news.index';
                $this->nextText = 'News';
                $this->nextTitle = 'Buka Halaman News';
                $this->prevRoute = 'home';
                $this->prevText = 'Home';
                $this->prevTitle = 'Buka Halaman Home';
            }
        } else if ($state == 'detail') {
            if ($menu == 'News') {
                $next = News::where('id', '>', $id)->orderBy('id', 'asc')->first();
                $prev = News::where('id', '<', $id)->orderBy('id', 'desc')->first();

                $this->nextRoute = 'news.detail';
                $this->nextText = 'Newer News';
                $this->nextTitle = $next->title ?? null;
                $this->nextId = $next->slug ?? null;
                $this->prevRoute = 'news.detail';
                $this->prevText = 'Older News';
                $this->prevTitle = $prev->title ?? null;
                $this->prevId = $prev->slug ?? null;
            } else if ($menu == 'Renungan') {
                $next = Renungan::whereDate('date', '>', $id)->first();
                $prev = Renungan::whereDate('date', '<', $id)->orderBy('date', 'desc')->first();

                $this->prevRoute = 'renungan.detail';
                $this->prevText = 'Older Renungan';
                $this->prevTitle = $prev->title ?? null;
                $this->prevId = $prev->slug ?? null;
                $this->nextRoute = 'renungan.detail';
                $this->nextText = 'Newer Renungan';
                $this->nextTitle = $next->title ?? null;
                $this->nextId = $next->slug ?? null;
            }
        }
    }

    public function render()
    {
        return view('livewire.nav-footer-component');
    }
}

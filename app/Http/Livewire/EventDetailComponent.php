<?php

namespace App\Http\Livewire;

use App\Models\Event;
use App\Models\EventPresence;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EventDetailComponent extends Component
{
    public $data;
    public $days, $hours, $mins, $secs;
    public $detail;
    public $now;
    public $mobile;
    public $username;
    public $iduser;
    public $admin;
    public $role;
    public $isAbsenced = false;

    public function mount($id, $mobile = 0, $username = '-', $admin = 0, $iduser = null)
    {
        $this->mobile = $mobile;
        $this->username = $username;
        $this->admin = $admin;
        $this->role = Auth::user()->role ?? $admin;
        $this->iduser = $iduser;
        $this->data = Event::where('slug', $id)->first();

        $this->iduser = $iduser ?? Auth::id();

        if (!$this->data) abort(404);
        $update = Event::where('slug', $id)->increment('read');
        $this->countdown();
    }

    public function render()
    {
        return view('livewire.event-detail-component')
            ->extends('web.app')
            ->layoutData([
                'title' => $this->data->name,
                'menu' => 'Event',
                'mobile' => $this->mobile,
                'data' => $this->data,
            ]);
    }

    public function countdown()
    {
        $checkAbsence = EventPresence::where('event', $this->data->id)->whereNotNull('user')->where('user', $this->iduser)->first();
        if ($checkAbsence) $this->isAbsenced = true;

        $now = Carbon::now('Asia/Jakarta');

        $start = $this->data->show ? $this->data->open : $this->data->start;
        $this->now = $now;
        $this->days = $start->diffInDays($now);
        $this->hours = $start->diffInHours($now) - (24 * $this->days);
        $this->mins = $start->diffInMinutes($now) - (24 * 60 * $this->days) - (60 * $this->hours);
        $this->secs = $start->diffInSeconds($now) - (24 * 3600 * $this->days) - (3600 * $this->hours) - (60 * $this->mins);

        if ($this->secs % 6 == 0 || $this->secs % 6 == 1) $this->detail = 'COMING SOON';
        else if ($this->secs % 6 == 2 || $this->secs % 6 == 3) $this->detail = 'STAY TUNED!';
        else $this->detail = $this->data->name;
    }
}

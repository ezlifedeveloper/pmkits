<?php

namespace App\Http\Livewire;

use App\Models\Kepengurusan;
use App\Models\RiwayatOrganisasi;
use App\Models\RiwayatPendidikan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ProfileComponent extends Component
{
    use WithFileUploads;

    public $input_start, $input_end;
    public $inputKepengurusan, $input_position, $input_description, $input_certificate;
    public $input_degree, $input_faculty, $input_department, $input_nrp;
    public $input_id;
    public $input_name, $input_email, $input_birth_place, $input_birth_date, $input_username,
        $input_address, $input_address_origin, $input_phone, $input_phone_emergency,
        $input_old_password, $input_password, $input_repassword;
    public $input_photo;

    public $openPendidikan = 0;
    public $openOrganisasi = 0;

    public function render()
    {
        return view('livewire.profile-component', [
            'data' => User::with(['organisasi', 'pendidikan', 'roleUser'])->where('id', Auth::id())->first(),
            'kepengurusan' => Kepengurusan::orderBy('start', 'desc')->get()
        ])->extends('web.app')
            ->layoutData([
                'title' => 'Profile',
                'menu' => 'Profile',
            ]);
    }

    public function closeAll()
    {
        $this->openPendidikan = false;
        $this->openOrganisasi = false;
        $this->resetInputFields();
    }

    public function setProfileVariable()
    {
        $data = Auth::user();
        $this->input_name = $data->name;
        $this->input_username = $data->username;
        $this->input_email = $data->email;
        $this->input_birth_place = $data->birth_place;
        $this->input_birth_date = $data->birth_date;
        $this->input_address = $data->address;
        $this->input_address_origin = $data->address_origin;
        $this->input_phone = $data->phone;
        $this->input_phone_emergency = $data->phone_emergency;
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_start', 'input_end', 'input_password', 'input_old_password',
            'inputKepengurusan', 'input_position', 'input_description', 'input_certificate',
            'input_degree', 'input_faculty', 'input_department', 'input_nrp', 'input_repassword',
            'input_name', 'input_email', 'input_birth_place', 'input_birth_date', 'input_username',
            'input_address', 'input_address_origin', 'input_phone', 'input_phone_emergency', 'input_photo'
        ]);
    }

    public function updatedInputKepengurusan()
    {
        $data = Kepengurusan::where('id', $this->inputKepengurusan)->first();
        $this->input_start = date('Y-m-d', strtotime($data->start));
        $this->input_end = date('Y-m-d', strtotime($data->end));
    }

    public function toggleModalPendidikan()
    {
        $this->openPendidikan = !$this->openPendidikan;
    }

    public function toggleModalOrganisasi()
    {
        $this->openOrganisasi = !$this->openOrganisasi;
    }

    public function editPendidikan($id)
    {
        $this->input_id = $id;
        $data = RiwayatPendidikan::where('id', $id)->first();
        $this->input_start = date('Y-m-d', strtotime($data->start));
        $this->input_end = date('Y-m-d', strtotime($data->graduate));
        $this->input_degree = $data->degree;
        $this->input_nrp = $data->nrp;
        $this->input_faculty = $data->faculty;
        $this->input_department = $data->department;
    }

    public function storePendidikan()
    {
        if (!$this->input_id) {
            RiwayatPendidikan::create([
                'start' => $this->input_start,
                'graduate' => $this->input_end,
                'degree' => strtoupper($this->input_degree),
                'nrp' => $this->input_nrp,
                'faculty' => $this->input_faculty,
                'department' => $this->input_department,
                'user' => Auth::id()
            ]);
        } else {
            RiwayatPendidikan::where('id', $this->input_id)
                ->update([
                    'start' => $this->input_start,
                    'graduate' => $this->input_end,
                    'degree' => strtoupper($this->input_degree),
                    'nrp' => $this->input_nrp,
                    'faculty' => $this->input_faculty,
                    'department' => $this->input_department
                ]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function deletePendidikan($id)
    {
        $data = RiwayatPendidikan::where('id', $id)->delete();

        $this->alert('success', 'Data Pendidikan berhasil dihapus');
        $this->closeAll();
    }

    public function editOrganisasi($id)
    {
        $this->input_id = $id;
        $data = RiwayatOrganisasi::where('id', $id)->first();
        $this->input_start = date('Y-m-d', strtotime($data->start));
        $this->input_end = date('Y-m-d', strtotime($data->finish));
        $this->inputKepengurusan = $data->kepengurusan;
        $this->input_position = $data->position;
        $this->input_description = $data->description;
        $this->input_certificate = $data->certificate;
    }

    public function storeOrganisasi()
    {
        if (!$this->input_id) {
            RiwayatOrganisasi::create([
                'start' => $this->input_start,
                'finish' => $this->input_end,
                'kepengurusan' => $this->inputKepengurusan,
                'position' => $this->input_position,
                'description' => $this->input_description,
                'user' => Auth::id()
            ]);
        } else {
            RiwayatOrganisasi::where('id', $this->input_id)
                ->update([
                    'start' => $this->input_start,
                    'finish' => $this->input_end,
                    'kepengurusan' => $this->inputKepengurusan,
                    'position' => $this->input_position,
                    'description' => $this->input_description
                ]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function storeProfile()
    {
        $this->validate([
            'input_name' => 'required|string',
            'input_username' => 'required|string',
            'input_email' => 'required',
        ]);

        $qrCode = QrCode::format("svg")
            ->size(500)
            ->merge('/public/PMK.png')
            ->generate($this->input_username, public_path() . '/qr/' . $this->input_username . ".svg");

        User::where('id', Auth::id())
            ->update([
                'name' => $this->input_name,
                'username' => $this->input_username,
                'email' => $this->input_email,
                'birth_place' => $this->input_birth_place,
                'birth_date' => $this->input_birth_date,
                'address' => $this->input_address,
                'address_origin' => $this->input_address_origin,
                'phone' => $this->input_phone,
                'phone_emergency' => $this->input_phone_emergency,
                'qr' => env("APP_URL") . '/qr/' . $this->input_username . ".svg",
            ]);

        if ($this->input_photo && !is_string($this->input_photo)) {
            $ext = '.' . $this->input_photo->getClientOriginalExtension();
            $this->input_photo->storeAs('user', Auth::id() . '-photo' . $ext);
            User::where('id', Auth::id())->update(['photo' => env('APP_URL') . '/file/user/' . Auth::id() . '-photo' . $ext]);
        }

        $this->alert('success', 'Profile berhasil diperbarui');
        $this->closeAll();
    }

    public function updatePassword()
    {
        if ($this->input_repassword == $this->input_password) {
            if (Hash::check($this->input_old_password, Auth::user()->password)) {
                User::where('id', Auth::id())->update(['password' => bcrypt($this->input_password)]);
                $this->alert('success', 'Password berhasil diperbarui');
            } else {
                $this->alert('error', 'Password lama salah');
            }
        } else {
            $this->alert('error', 'Password tidak sama');
        }

        $this->closeAll();
    }

    public function deleteOrganisasi($id)
    {
        $data = RiwayatOrganisasi::where('id', $id)->delete();

        $this->alert('success', 'Data Organisasi berhasil dihapus');
        $this->closeAll();
    }
}

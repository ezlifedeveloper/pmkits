<?php

namespace App\Http\Livewire;

use App\Models\ProfilPMK;
use Livewire\Component;

class SocialMediaComponent extends Component
{
    public function render()
    {
        return view('livewire.social-media-component', [
            'data' => ProfilPMK::orderBy('id', 'desc')->first()
        ]);
    }
}

<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class LoginComponent extends Component
{
    public $username, $password;

    public function render()
    {
        return view('livewire.login-component')
            ->extends('web.app')
            ->section('login')
            ->layoutData([
                'title' => 'Login',
                'menu' => 'Login',
            ]);
    }

    public function login()
    {
        if (Auth::attempt(['email' => $this->username, 'password' => $this->password]) || Auth::attempt(['username' => $this->username, 'password' => $this->password])) {
            $this->alert('success', 'Login Berhasil', [
                'position' => 'top',
                'timer' =>  3000,
                'toast' => true
            ]);
            $user = Auth::user();
            if ($user->roleUser->name == 'Superuser') return redirect()->route('dashboard');
            else return redirect()->route('home');
        } else {
            return $this->alert('error', 'Terjadi kesalahan dalam login. Cek kembali Username dan Password Anda', [
                'position' => 'top',
                'timer' =>  3000,
                'toast' => true
            ]);
        }
    }
}

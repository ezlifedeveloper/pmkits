<?php

namespace App\Http\Livewire\Admin;

use App\Models\Kepengurusan;
use App\Models\RiwayatOrganisasi;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class RiwayatOrganisasiManagement extends Component
{
    use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';

    public $input_start, $input_end;
    public $inputKepengurusan, $input_position, $input_description, $input_certificate;
    public $input_id;
    public $input_user;
    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;

        return view('livewire.admin.riwayat-organisasi-management', [
            'data' => RiwayatOrganisasi::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('position', 'like', '%' . $searchData . '%')
                        ->orWhere('description', 'like', '%' . $searchData . '%');
                });
            })->orderBy('status')
                ->orderBy('certificate', 'asc')
                ->orderBy('start')
                ->paginate(20),
            'kepengurusan' => Kepengurusan::orderBy('start', 'desc')->get(),
            'user' => User::where('role', '<>', 1)->orderBy('name')->get()
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Riwayat Organisasi',
            ]);
    }

    public function closeAll()
    {
        $this->openOrganisasi = false;
        $this->resetInputFields();
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_start', 'input_end', 'input_user',
            'inputKepengurusan', 'input_position', 'input_description', 'input_certificate',
        ]);
    }

    public function updatedInputKepengurusan()
    {
        $data = Kepengurusan::where('id', $this->inputKepengurusan)->first();
        $this->input_start = date('Y-m-d', strtotime($data->start));
        $this->input_end = date('Y-m-d', strtotime($data->end));
    }

    public function editOrganisasi($id)
    {
        $this->input_id = $id;
        $data = RiwayatOrganisasi::where('id', $id)->first();
        $this->input_start = date('Y-m-d', strtotime($data->start));
        $this->input_end = date('Y-m-d', strtotime($data->finish));
        $this->inputKepengurusan = $data->kepengurusan;
        $this->input_position = $data->position;
        $this->input_description = $data->description;
        $this->input_certificate = $data->certificate;
        $this->input_user = $data->user;
    }

    public function approve($id, $status)
    {
        RiwayatOrganisasi::where('id', $id)->update([
            'status' => !$status,
        ]);

        $this->alert('success', 'Data berhasil ' . ($status ? 'ditolak' : 'disetujui'));
        $this->closeAll();
    }

    public function storeCertificate()
    {
        if ($this->input_certificate && !is_string($this->input_certificate)) {
            $ext = '.' . $this->input_certificate->getClientOriginalExtension();
            $this->input_certificate->storeAs('user', $this->inputKepengurusan . $this->input_user . '-' . $this->input_position . '-certificate' . $ext);
            RiwayatOrganisasi::where('id', $this->input_id)->update(['certificate' => env('APP_URL') . '/file/user/' . $this->inputKepengurusan . $this->input_user . '-' . $this->input_position . '-certificate' . $ext]);

            $this->alert('success', 'Sertifikat berhasil diunggah');
            $this->closeAll();
        }
    }

    public function storeOrganisasi()
    {
        if (!$this->input_id) {
            RiwayatOrganisasi::create([
                'start' => $this->input_start,
                'finish' => $this->input_end,
                'kepengurusan' => $this->inputKepengurusan,
                'position' => $this->input_position,
                'description' => $this->input_description,
                'user' => $this->input_user
            ]);
        } else {
            RiwayatOrganisasi::where('id', $this->input_id)
                ->update([
                    'start' => $this->input_start,
                    'finish' => $this->input_end,
                    'kepengurusan' => $this->inputKepengurusan,
                    'position' => $this->input_position,
                    'description' => $this->input_description,
                    'user' => $this->input_user
                ]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function deleteOrganisasi($id)
    {
        $data = RiwayatOrganisasi::where('id', $id)->delete();

        $this->alert('success', 'Data Organisasi berhasil dihapus');
        $this->closeAll();
    }
}

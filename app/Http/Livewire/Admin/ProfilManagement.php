<?php

namespace App\Http\Livewire\Admin;

use App\Models\ProfilPMK;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProfilManagement extends Component
{
    use WithFileUploads;

    public $isEdit = 0;
    public $homeCover;
    public $input_vision, $input_mission, $input_detail, $input_address, $input_instagram,
        $input_line, $input_phone, $input_email, $input_youtube, $input_facebook, $input_twitter;
    public $homecover, $banner;

    public function render()
    {
        return view('livewire.admin.profil-management', [
            'data' => ProfilPMK::first(),
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Profil',
            ]);
    }

    public function editData()
    {
        $data = ProfilPMK::first();
        $this->input_vision = $data->vision;
        $this->input_mission = $data->mission;
        $this->input_detail = $data->detail;
        $this->input_address = $data->address;
        $this->input_instagram = $data->instagram;
        $this->input_line = $data->line;
        $this->input_phone = $data->phone;
        $this->input_email = $data->email;
        $this->input_youtube = $data->youtube;
        $this->input_facebook = $data->facebook;
        $this->input_twitter = $data->twitter;
        $this->banner = $data->pagebanner;
        $this->homecover = $data->homecover;

        $this->isEdit = true;
    }

    public function saveData()
    {
        $update = ProfilPMK::where('id', 1)->update([
            'vision' => $this->input_vision,
            'mission' => $this->input_mission,
            'detail' => $this->input_detail,
            'address' => $this->input_address,
            'instagram' => $this->input_instagram,
            'line' => $this->input_line,
            'phone' => $this->input_phone,
            'email' => $this->input_email,
            'youtube' => $this->input_youtube,
            'facebook' => $this->input_facebook,
            'twitter' => $this->input_twitter,
        ]);

        if ($this->homecover && !is_string($this->homecover)) {
            $ext = '.' . $this->homecover->getClientOriginalExtension();
            $this->homecover->storeAs('profilepmk', 'profile-homecover' . $ext);
            ProfilPMK::where('id', 1)->update(['homecover' => env('APP_URL') . '/file/profilepmk/' . 'profile-homecover' . $ext]);
        }

        if ($this->banner && !is_string($this->banner)) {
            $ext = '.' . $this->banner->getClientOriginalExtension();
            $this->banner->storeAs('profilepmk', 'profile-banner' . $ext);
            ProfilPMK::where('id', 1)->update(['pagebanner' => env('APP_URL') . '/file/profilepmk/' . 'profile-banner' . $ext]);
        }

        $this->alert('success', 'Data berhasil disimpan');

        $this->reset([
            'input_vision', 'input_mission', 'input_detail', 'input_address', 'input_instagram',
            'input_line', 'input_phone', 'input_email', 'input_youtube', 'input_facebook', 'input_twitter',
            'banner', 'homecover'
        ]);
        $this->isEdit = false;
    }
}

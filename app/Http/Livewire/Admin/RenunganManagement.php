<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Renungan;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class RenunganManagement extends Component
{
    use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';

    public $showData = 0;
    public $showDetail = 0;

    public $input_id;
    public $input_source, $input_description, $input_type, $input_tag,
        $input_time, $qrCode, $input_title, $input_date, $input_verse;
    public $searchTerm;
    public $cover, $hdcover;

    public function render()
    {
        $searchData = $this->searchTerm;

        return view('livewire.admin.renungan-management', [
            'data' => Renungan::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('title', 'like', '%' . $searchData . '%')
                        ->orWhere('tag', 'like', '%' . $searchData . '%')
                        ->orWhere('source', 'like', '%' . $searchData . '%');
                });
            })->orderBy('date', 'desc')->paginate(10)
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Renungan',
            ]);
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_description', 'input_date',
            'input_tag', 'cover', 'input_title',
            'input_verse', 'input_source'
        ]);
    }

    public function closeAll()
    {
        $this->showData = false;
        $this->showDetail = false;
        $this->resetInputFields();
    }

    public function createData()
    {
        $this->showData = true;
        $this->showDetail = false;
    }

    public function openData($id)
    {
        $this->input_id = $id;
        $this->showData = true;
        $this->showDetail = false;

        $data = Renungan::findOrFail($id);
        $this->input_description = $data->description;
        $this->input_tag = $data->tag;
        $this->input_title = $data->title;
        $this->input_verse = $data->verse;
        $this->input_date = date('Y-m-d', strtotime($data->date));
        $this->input_source = $data->source;
        $this->cover = $data->cover;
        $this->qrCode = $data->qr;
    }

    public function storeData()
    {
        $this->validate([
            'input_title' => 'required|string',
            'input_verse' => 'required|string',
            'input_source' => 'required|string',
            'input_description' => 'required|string',
        ]);

        $kepengurusan = Helper::getIdKepengurusanactive();
        $slug = strtolower(Helper::cleanStr($this->input_title . '-' . $kepengurusan));
        if ($this->input_id) {
            Renungan::where('id', $this->input_id)->update([
                'slug' => strtolower(str_replace(' ', '-', $slug)),
                'description' => $this->input_description,
                'excerpt' => substr($this->input_description, 0, 120) . '...',
                'verse' => $this->input_verse,
                'tag' => $this->input_tag,
                'date' => $this->input_date,
                'source' => $this->input_source,
                'title' => $this->input_title,
            ]);
        } else {
            $data = Renungan::create([
                'slug' => strtolower(str_replace(' ', '-', $slug)),
                'description' => $this->input_description,
                'excerpt' => substr($this->input_description, 0, 120) . '...',
                'verse' => $this->input_verse,
                'tag' => $this->input_tag,
                'source' => $this->input_source,
                'title' => $this->input_title,
                'date' => $this->input_date,
                'kepengurusan' => $kepengurusan,
                'created_by' => Auth::id()
            ]);
        }

        $qrCode = QrCode::format("svg")
            ->size(500)
            ->merge('/public/PMK.png')
            ->generate(env("APP_URL") . '/renungan/' . $slug, public_path() . '/qr/' . $slug . ".svg");

        $id = $this->input_id ?? $data->id;
        Renungan::where('id', $id)->update([
            'qr' => env("APP_URL") . '/qr/' . $slug . ".svg",
        ]);

        if ($this->cover && !is_string($this->cover)) {
            $ext = '.' . $this->cover->getClientOriginalExtension();
            $this->cover->storeAs('renungan', $slug . '-cover' . $ext);
            Renungan::where('id', $id)->update(['cover' => env('APP_URL') . '/file/renungan/' . $slug . '-cover' . $ext]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function delete($id)
    {
        $data = Renungan::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}

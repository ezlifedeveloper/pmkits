<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Event;
use App\Models\EventPresence;
use App\Models\EventType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class EventManagement extends Component
{
    use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';

    public $showEvent = 0;
    public $showDetail = 0;
    public $editReport = 0;

    public $input_id;
    public $input_name, $input_description, $input_tag, $input_end, $input_report,
        $input_start, $input_open, $input_close, $qrCode, $input_title, $input_absence_type,
        $input_speaker, $input_location, $input_url, $input_verse, $input_show, $input_transfer;
    public $searchTerm;
    public $cover, $hdcover;
    public $event;

    public function render()
    {
        $role = Auth::user()->role;
        $searchData = $this->searchTerm;

        return view('livewire.admin.event-management', [
            'data' => Event::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('name', 'like', '%' . $searchData . '%')
                        ->orWhere('tag', 'like', '%' . $searchData . '%');
                });
            })->orderBy('start', 'desc')->paginate(10, ['*'], 'data')
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Event',
            ]);
    }

    public function mount()
    {
        $this->input_type = '1';
        $this->input_show = 1;
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_name', 'input_description', 'input_show', 'input_transfer',
            'input_tag', 'input_start', 'cover', 'input_end', 'input_report',
            'hdcover', 'input_open', 'input_close', 'input_title', 'input_absence_type',
            'input_speaker', 'input_location', 'input_url', 'input_verse',
        ]);

        $this->input_type = '1';
        $this->input_show = 1;
        $this->input_absence_type = 'Absence';
        $this->input_transfer = 0;
    }

    public function closeAll()
    {
        $this->showEvent = false;
        $this->showDetail = false;
        $this->resetInputFields();
    }

    public function createEvent()
    {
        $this->showEvent = true;
        $this->showDetail = false;
    }

    public function openEvent($id)
    {
        $this->input_id = $id;
        $this->showEvent = true;
        $this->showDetail = false;

        $data = Event::findOrFail($id);
        $this->input_name = $data->name;
        $this->input_description = $data->description;
        $this->input_tag = $data->tag;
        $this->input_title = $data->title;
        $this->input_show = $data->show;
        $this->input_speaker = $data->speaker;
        $this->input_location = $data->location;
        $this->input_verse = $data->verse;
        $this->input_url = $data->url;
        $this->input_transfer = $data->transfer;
        $this->input_absence_type = $data->absence_type;
        $this->cover = $data->cover;
        $this->hdcover = $data->hdcover;
        $this->input_start = date('Y-m-d\TH:i:s', strtotime($data->start));
        $this->input_end = date('Y-m-d\TH:i:s', strtotime($data->end));
        $this->input_open = date('Y-m-d\TH:i:s', strtotime($data->open));
        $this->input_close = date('Y-m-d\TH:i:s', strtotime($data->close));
        $this->qrCode = $data->qr;
    }

    public function openDetail($id)
    {
        $this->input_id = $id;
        $this->event = Event::findOrFail($id);
        $this->showEvent = false;
        $this->showDetail = true;
    }

    public function storeEvent()
    {
        $this->validate([
            'input_name' => 'required|string',
            'input_description' => 'required|string',
            'input_start' => 'required',
        ]);

        $kepengurusan = Helper::getIdKepengurusanactive();
        $slug = strtolower(Helper::cleanStr($this->input_name . '-' . $kepengurusan));
        if ($this->input_id) {
            Event::where('id', $this->input_id)->update([
                'name' => $this->input_name,
                'slug' => strtolower(str_replace(' ', '-', $slug)),
                'description' => $this->input_description,
                'excerpt' => substr($this->input_description, 0, 97) . '...',
                'access_link' => env("APP_URL") . '/event/' . $slug,
                'absence_link' => env("APP_URL") . '/attend/' . $slug,
                'start' => $this->input_start,
                'end' => $this->input_end,
                'open' => $this->input_open,
                'close' => $this->input_close,
                'tag' => $this->input_tag,
                'title' => $this->input_title,
                'transfer' => $this->input_transfer,
                'absence_type' => $this->input_absence_type,
                'speaker' => $this->input_speaker,
                'verse' => $this->input_verse,
                'show' => $this->input_show,
                'url' => $this->input_url,
                'location' => $this->input_location,
            ]);
        } else {
            $data = Event::create([
                'name' => $this->input_name,
                'slug' => strtolower(str_replace(' ', '-', $slug)),
                'description' => $this->input_description,
                'excerpt' => substr($this->input_description, 0, 97) . '...',
                'access_link' => env("APP_URL") . '/event/' . $slug,
                'absence_link' => env("APP_URL") . '/attend/' . $slug,
                'start' => $this->input_start,
                'end' => $this->input_end,
                'open' => $this->input_open,
                'close' => $this->input_close,
                'tag' => $this->input_tag,
                'title' => $this->input_title,
                'transfer' => $this->input_transfer,
                'absence_type' => $this->input_absence_type,
                'speaker' => $this->input_speaker,
                'verse' => $this->input_verse,
                'show' => $this->input_show,
                'url' => $this->input_url,
                'location' => $this->input_location,
                'kepengurusan' => $kepengurusan,
                'created_by' => Auth::id()
            ]);
        }

        $qrCode = QrCode::format("svg")
            ->size(500)
            ->merge('/public/PMK.png')
            ->generate(env("APP_URL") . '/event/' . $slug, public_path() . '/qr/' . $slug . ".svg");

        $id = $this->input_id ?? $data->id;
        Event::where('id', $id)->update([
            'qr' => env("APP_URL") . '/qr/' . $slug . ".svg",
        ]);

        if ($this->cover && !is_string($this->cover)) {
            $ext = '.' . $this->cover->getClientOriginalExtension();
            $this->cover->storeAs('event', $slug . '-cover' . $ext);
            Event::where('id', $id)->update(['cover' => env('APP_URL') . '/file/event/' . $slug . '-cover' . $ext]);
        }

        if ($this->hdcover && !is_string($this->hdcover)) {
            $ext = '.' . $this->hdcover->getClientOriginalExtension();
            $this->hdcover->storeAs('event', $slug . '-hdcover' . $ext);
            Event::where('id', $id)->update(['isFeatured' => true, 'hdcover' => env('APP_URL') . '/file/event/' . $slug . '-hdcover' . $ext]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function delete($id)
    {
        $event = Event::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}

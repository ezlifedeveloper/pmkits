<?php

namespace App\Http\Livewire\Admin;

use App\Models\PrayerRequest;
use Livewire\Component;
use Livewire\WithPagination;

class PrayerRequestManagement extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $searchTerm;

    public $showPrayReq=0;

    public $input_id,$input_user,$input_prayer,$input_report, $input_visible, $input_showed,$input_prayed;


    public function render()
    {
        $searchData = $this->searchTerm;

        return view('livewire.admin.prayer-request-management', [
            'data' => PrayerRequest::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('prayer', 'like', '%' . $searchData . '%');
                });
            })->orderBy('created_at', 'desc')->paginate(10)
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Prayer Request',
            ]);
    }

    public function openPrayReq($id)
    {
        $this->input_id = $id;
        $this->showPrayReq = true;

        $prayerReq = PrayerRequest::findOrFail($id);
        $this->input_user = $prayerReq->user;
        $this->input_prayer = $prayerReq->prayer;
        $this->input_report = $prayerReq->report;
        $this->input_visible = $prayerReq->visible;
        $this->input_showed = $prayerReq->showed;
        $this->input_prayed = $prayerReq->prayed;
    }

    public function storePray()
    {
        // $this->validate();

        if($this->input_id) {
            PrayerRequest::where('id', $this->input_id)->update([
                'prayer' => $this->input_user,
                'report' => $this->input_report,
            ]);
        };
    }

    public function setActive($id)
    {
        $activate = PrayerRequest::where('id', $id)->update(['prayed' => 1]);
        $this->alert('success', 'Ubah status data berhasil');
    }

    public function delete($id)
    {
        $data = PrayerRequest::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
    }
    public function closeAll()
    {
        $this->showPrayReq = false;
    }
}

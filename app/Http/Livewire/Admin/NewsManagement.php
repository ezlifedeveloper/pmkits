<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\News;
use App\Models\NewsFile;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class NewsManagement extends Component
{
    use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';

    public $showNews = 0;
    public $showDetail = 0;
    public $showFile = 0;

    public $input_id;
    public $input_name, $input_description, $input_type, $input_tag,
        $input_time, $input_open, $input_close, $qrCode, $input_title,
        $input_speaker, $input_location, $input_url, $input_verse;
    public $searchTerm;
    public $cover, $hdcover;
    public $news;
    public $input_file;
    public $input_photo;

    public function render()
    {
        $role = Auth::user()->role;
        $searchData = $this->searchTerm;

        return view('livewire.admin.news-management', [
            'data' => News::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('title', 'like', '%' . $searchData . '%')
                        ->orWhere('tag', 'like', '%' . $searchData . '%');
                });
            })->orderBy('created_at', 'desc')->paginate(10)
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'News',
            ]);
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_description',
            'input_tag', 'cover', 'input_title',
            'input_url', 'input_photo', 'input_file'
        ]);

        $this->input_type = '1';
    }

    public function closeAll()
    {
        $this->showNews = false;
        $this->showDetail = false;
        $this->showFile = false;
        $this->resetInputFields();
    }

    public function createNews()
    {
        $this->showNews = true;
        $this->showDetail = false;
        $this->showFile = false;
    }

    public function createFile($id)
    {
        $this->input_id = $id;
        $this->showNews = false;
        $this->showDetail = false;
        $this->showFile = true;
    }

    public function openNews($id)
    {
        $this->input_id = $id;
        $this->showNews = true;
        $this->showDetail = false;

        $data = News::findOrFail($id);
        $this->input_description = $data->description;
        $this->input_tag = $data->tag;
        $this->input_title = $data->title;
        $this->input_url = $data->url;
        $this->cover = $data->cover;
        $this->qrCode = $data->qr;
    }

    public function openDetail($id)
    {
        $this->input_id = $id;
        $this->news = News::findOrFail($id);
        $this->showNews = false;
        $this->showDetail = true;
        $this->showFile = false;
    }

    public function storeNews()
    {
        $this->validate([
            'input_title' => 'required|string',
            'input_description' => 'required|string',
        ]);

        $kepengurusan = Helper::getIdKepengurusanactive();
        $slug = strtolower(Helper::cleanStr($this->input_title . '-' . $kepengurusan));
        if ($this->input_id) {
            News::where('id', $this->input_id)->update([
                'slug' => strtolower(str_replace(' ', '-', $slug)),
                'description' => $this->input_description,
                'excerpt' => substr($this->input_description, 0, 97) . '...',
                'tag' => $this->input_tag,
                'title' => $this->input_title,
                'url' => $this->input_url,
            ]);
        } else {
            $data = News::create([
                'slug' => strtolower(str_replace(' ', '-', $slug)),
                'description' => $this->input_description,
                'excerpt' => substr($this->input_description, 0, 97) . '...',
                'tag' => $this->input_tag,
                'title' => $this->input_title,
                'url' => $this->input_url,
                'kepengurusan' => $kepengurusan,
                'created_by' => Auth::id()
            ]);
        }

        $qrCode = QrCode::format("svg")
            ->size(500)
            ->merge('/public/PMK.png')
            ->generate(env("APP_URL") . '/news/' . $slug, public_path() . '/qr/' . $slug . ".svg");

        $id = $this->input_id ?? $data->id;
        News::where('id', $id)->update([
            'qr' => env("APP_URL") . '/qr/' . $slug . ".svg",
        ]);

        if ($this->cover && !is_string($this->cover)) {
            $ext = '.' . $this->cover->getClientOriginalExtension();
            $this->cover->storeAs('news', $slug . '-cover' . $ext);
            News::where('id', $id)->update(['cover' => env('APP_URL') . '/file/news/' . $slug . '-cover' . $ext]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function uploadFile($id)
    {
        $ext = '.' . $this->input_photo->getClientOriginalExtension();
        $name = $this->input_file;
        $fileName = time() . '_' . strtolower(preg_replace('/\s+/', '_', $this->input_file));
        $this->input_photo->storeAs('news', $fileName . $ext);

        NewsFile::create([
            'news' => $id,
            'name' => $name,
            'created_by' => Auth::id(),
            'file' => env('APP_URL') . '/file/news/' . $fileName . $ext,
        ]);

        $this->alert('success', 'File berhasil disimpan');
        $this->closeAll();
    }

    public function deleteFile($id)
    {
        $data = NewsFile::where('id', $id)->delete();

        $this->alert('success', 'File berhasil dihapus');
        $this->closeAll();
    }

    public function delete($id)
    {
        $data = News::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}

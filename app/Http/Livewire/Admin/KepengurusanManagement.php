<?php

namespace App\Http\Livewire\Admin;

use App\Models\Kepengurusan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class KepengurusanManagement extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $showData = 0;
    public $showDetail = 0;

    public $input_id;
    public $input_name, $input_start, $input_type, $input_end;
    public $searchTerm;
    public $cover, $hdcover;

    public function render()
    {
        $searchData = $this->searchTerm;

        return view('livewire.admin.kepengurusan-management', [
            'data' => Kepengurusan::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('name', 'like', '%' . $searchData . '%');
                });
            })->orderBy('start', 'desc')->paginate(10)
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'Kepengurusan',
            ]);
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_start',
            'input_end', 'input_name',
        ]);

        $this->input_type = '1';
    }

    public function closeAll()
    {
        $this->showData = false;
        $this->showDetail = false;
        $this->resetInputFields();
    }

    public function createData()
    {
        $this->showData = true;
        $this->showDetail = false;
    }

    public function openData($id)
    {
        $this->input_id = $id;
        $this->showData = true;
        $this->showDetail = false;

        $data = Kepengurusan::findOrFail($id);
        $this->input_start = $data->start;
        $this->input_end = $data->end;
        $this->input_name = $data->name;
    }

    public function setActive($id)
    {
        $deactivate = DB::table('kepengurusans')->update(['status' => 0]);
        $activate = Kepengurusan::where('id', $id)->update(['status' => 1]);
    }

    public function storeData()
    {
        $this->validate([
            'input_name' => 'required|string',
            'input_start' => 'required|string',
        ]);

        if ($this->input_id) {
            Kepengurusan::where('id', $this->input_id)->update([
                'start' => $this->input_start,
                'end' => $this->input_end,
                'name' => $this->input_name,
            ]);
        } else {
            $deactivate = DB::table('kepengurusans')->update(['status' => 0]);
            $data = Kepengurusan::create([
                'start' => $this->input_start,
                'end' => $this->input_end,
                'name' => $this->input_name,
                'status' => 1,
                'created_by' => Auth::id()
            ]);
        }

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function delete($id)
    {
        $data = Kepengurusan::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}

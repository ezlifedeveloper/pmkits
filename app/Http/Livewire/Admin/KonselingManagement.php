<?php

namespace App\Http\Livewire\Admin;

use App\Models\Counseling;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class KonselingManagement extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $showData = 0;
    public $showDetail = 0;

    public $input_id;
    public $input_user, $input_status, $input_start, $input_type;
    public $searchTerm;
    public $cover, $hdcover;


    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.admin.konseling-management', [
        'data' => Counseling::when($searchData, function ($searchQuery) use ($searchData) {
            $searchQuery->where(function ($searchQuery) use ($searchData) {
                $searchQuery->where('name', 'like', '%' . $searchData . '%');
            });
        })->orderBy('id', 'desc')->paginate(10)
    ])->extends('admin.app')
        ->layoutData([
            'title' => 'Konseling',
        ]);
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_user',
            'input_status', 'input_start',
        ]);

        $this->input_type = '1';
    }

    public function closeAll()
    {
        $this->showData = false;
        $this->showDetail = false;
        $this->resetInputFields();
    }

    public function createData()
    {
        $this->showData = true;
        $this->showDetail = false;
    }

    // public function openData($id)
    // {
    //     $this->input_id = $id;
    //     $this->showData = true;
    //     $this->showDetail = false;

    //     $data = Counseling::findOrFail($id);
    //     $this->input_start = $data->start;
    //     $this->input_type = $data->type;
    //     $this->input_user = $data->user;
    // }

    // public function setActive($id)
    // {
    //     $deactivate = DB::table('counselings')->update(['status' => 0]);
    //     $activate = Counseling::where('id', $id)->update(['status' => 1]);
    // }

    // public function storeData()
    // {

    //     if ($this->input_id) {
    //         Counseling::where('id', $this->input_id)->update([
    //             'start' => $this->input_start,
    //             'type' => $this->input_type,
    //             'user' => $this->input_user,
    //         ]);
    //     } else {
    //         $deactivate = DB::table('counselings')->update(['status' => 0]);
    //         $data = Counseling::create([
    //             'start' => $this->input_start,
    //             'type' => $this->input_type,
    //             'user' => $this->input_user,
    //             'status' => 1,
    //             'created_by' => Auth::id()
    //         ]);
    //     }

    //     $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
    //     $this->closeAll();
    // }

    public function delete($id)
    {
        $data = Counseling::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Models\Event;
use Livewire\Component;

class EventDetailComponent extends Component
{
    public $eventid;
    public $editReport = 0;
    public $input_report;

    public function mount($eventid)
    {
        $this->eventid = $eventid;
    }

    public function render()
    {
        return view('livewire.admin.event-detail-component', [
            'event' => Event::where('id', $this->eventid)->first()
        ]);
    }

    public function storeReport($id)
    {
        Event::where('id', $id)->update(['report' => $this->input_report]);
        $this->event = Event::findOrFail($id);
        $this->editReport = 0;

        $this->alert('success', 'Report berhasil disimpan');
    }

    public function openReport($id)
    {
        $d = Event::findOrFail($id);
        $this->input_report = $d->report;
        $this->editReport = 1;
    }
}

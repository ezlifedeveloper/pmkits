<?php

namespace App\Http\Livewire\Admin;

use App\Models\Event;
use App\Models\EventImage;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class GalleryComponent extends Component
{
    use WithFileUploads;

    public $eventid;
    public $isOpen = 0;
    public $image;

    public function mount($eventid)
    {
        $this->eventid = $eventid;
    }

    public function render()
    {
        return view('livewire.admin.gallery-component', [
            'gallery' => EventImage::where('event', $this->eventid)->get()
        ]);
    }

    public function storeImage()
    {
        $event = Event::where('id', $this->eventid)->first();
        $slug = $event->slug;
        if ($this->image && !is_string($this->image)) {
            $ext = '.' . $this->image->getClientOriginalExtension();
            $this->image->storeAs('event', time() . '-' . $slug . '-image' . $ext);

            EventImage::create([
                'event' => $this->eventid,
                'name' => time() . '-' . $slug . '-image' . $ext,
                'file' => env('APP_URL') . '/file/event/' . time() . '-' . $slug . '-image' . $ext,
                'created_by' => Auth::id()
            ]);
        }

        $this->alert('success', 'Gambar berhasil disimpan');
        $this->isOpen = 0;
    }

    public function addImage()
    {
        $this->isOpen = 1;
    }

    public function delete($id)
    {
        $event = EventImage::where('id', $id)->delete();

        $this->alert('success', 'Gambar berhasil dihapus');
        $this->isOpen = 0;
    }
}

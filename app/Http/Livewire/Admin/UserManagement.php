<?php

namespace App\Http\Livewire\Admin;

use App\Exports\UserExport;
use App\Imports\UserImport;
use App\Models\User;
use App\Models\UserType;
use Clockwork\Request\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Expr\FuncCall;

class UserManagement extends Component
{
    use WithPagination;
    use WithFileUploads;
    protected $paginationTheme = 'bootstrap';

    public $showUser = 0;
    public $showActivity = 0;
    public $showImport = 0;
    public $importName;

    public $input_id;
    public $input_name, $input_username, $input_role, $input_password, $input_email, $qrCode ;
    public $input_access = [];
    // public $access_implode = implode(", ", $this->input_access);
    public $searchTerm;


    public function render()
    {
        $role = Auth::user()->role;
        $searchData = $this->searchTerm;

        return view('livewire.admin.user-management', [
            'type' => UserType::all(),
            'data' => User::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('name', 'like', '%' . $searchData . '%')
                        ->orWhere('username', 'like', '%' . $searchData . '%')
                        ->orWhere('email', 'like', '%' . $searchData . '%');
                });
            })->paginate(10)
        ])->extends('admin.app')
            ->layoutData([
                'title' => 'User',
            ]);
    }

    public function mount()
    {
        $this->input_role = '1';
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_name', 'input_username',
            'input_role', 'input_password', 'input_email'
        ]);

        $this->input_role = 'administrator';
    }


    public function closeAll()
    {
        $this->showUser = false;
        $this->showActivity = false;
        $this->showImport = false;
        $this->resetInputFields();
    }

    public function exportExcel()
    {
        return Excel::download(new UserExport, 'user.xlsx');
    }

    public function openImport()
    {
        $this->showImport = true;

    }

    public function storeImport()
    {
        $this->validate([
            'importName' => 'required|mimes:xlsx,xls,csv'
        ]);

        Excel::import(new UserImport, $this->importName);

        $this->alert('success', 'Berhasil Import');
        $this->closeAll();

    }

    public function createUser()
    {
        $this->showUser = true;
        $this->showActivity = false;
    }

    public function openUser($id)
    {
        $this->input_id = $id;
        $this->showUser = true;
        $this->showActivity = false;

        $user = User::findOrFail($id);
        $this->input_name = $user->name;
        $this->input_username = $user->username;
        $this->input_email = $user->email;
        $this->input_role = $user->role;
        $this->qrCode = $user->qr;
    }

    public function openActivity($id)
    {
        $this->input_id = $id;
        $this->showUser = false;
        $this->showActivity = true;
    }

    public function storeUser()
    {
        $this->validate([
            'input_name' => 'required|string',
            'input_username' => 'required|string',
            'input_role' => 'required',
            'input_email' => 'required',
        ]);

        if ($this->input_id) {
            User::where('id', $this->input_id)->update([
                'name' => $this->input_name,
                'username' => $this->input_username,
                'email' => $this->input_email,
                'role' => $this->input_role,
            ]);

            if ($this->input_password) {
                User::where('id', $this->input_id)->update([
                    'password' => bcrypt($this->input_password),
                ]);
            }
        } else {
            $data = User::create([
                'name' => $this->input_name,
                'username' => $this->input_username,
                'role' => $this->input_role,
                'email' => $this->input_email,
                'password' => bcrypt($this->input_password ?? 123),
                'access'=> implode(", ", $this->input_access),
                // 'access'=> (count($this->input_access) > 1 ? implode(", ", $this->input_access) : $this->input_access),
                'created_by' => Auth::id()
            ]);
        }

        $qrCode = QrCode::format("svg")
            ->size(500)
            ->merge('/public/PMK.png')
            ->generate($this->input_username, public_path() . '/qr/' . $this->input_username . ".svg");

        $id = $this->input_id ?? $data->id;
        User::where('id', $id)->update([
            'qr' => env("APP_URL") . '/qr/' . $this->input_username . ".svg",
        ]);

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeAll();
    }

    public function resetPassword($id)
    {
        $user = User::where('id', $id)->first();
        User::where('id', $id)->update([
            'password' => bcrypt($user->email),
        ]);

        $this->alert('success', 'Password di reset menjadi email user');
        $this->closeAll();
    }

    public function setStatus($id, $status)
    {
        User::where('id', $id)->update([
            'status' => !$status,
        ]);

        $this->alert('success', 'Data berhasil diupdate');
        $this->closeAll();
    }

    public function delete($id)
    {
        $user = User::where('id', $id)->delete();

        $this->alert('success', 'Data berhasil dihapus');
        $this->closeAll();
    }
}

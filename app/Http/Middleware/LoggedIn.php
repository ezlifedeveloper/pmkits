<?php

namespace App\Http\Middleware;

use Closure;
use DASPRiD\Enum\NullValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;

class LoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if (Auth::check()) {
            $user = Auth::user();

            if ($user->roleUser->name == 'Superuser' || in_array($user->roleUser->name, $roles)) {
                return $next($request);
            }elseif($user->roleUser->name == 'Mahasiswa' && $user->access!=null){
                return $next($request);
            }else {
                return redirect()->route('home');
            }
        }
        return redirect()->route('login');
    }
}


// elseif ($user->roleUser->name == 'Mahasiswa' || in_array($user->roleUser->name, $roles)) {
//     return $next($request);
// }
// }elseif($user->roleUser->name == 'Mahasiswa' ){
//     return $next($request);
// }

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $access)
    {

        if (Auth::check()) {
            $user = Auth::user();

            if($user->roleUser->name == 'Mahasiswa' && str_contains($user->access, $access)){
                return $next($request);
            }else {
                return redirect()->route('home');
            }
        }
        return redirect()->route('login');
    }
}

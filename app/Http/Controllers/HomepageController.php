<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\News;
use App\Models\ProfilPMK;
use App\Models\Renungan;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $event = Event::whereDate('start', '>=', now())->orderBy('start', 'asc')->limit(3)->get();
        if (count($event) < 1) $data['event'] = Event::orderBy('start', 'desc')->limit(3)->get();
        else $data['event'] = $event;
        $data['renungan'] = Renungan::whereDate('date', '<=', now())->orderBy('date', 'desc')->limit(3)->get();
        $data['news'] = News::orderBy('created_at', 'desc')->limit(2)->get();
        $data['profil'] = ProfilPMK::orderBy('created_at', 'desc')->first();

        return view('web.welcome')
            ->with('data', $data)
            ->with('title', 'Home')
            ->with('menu', 'Home');
    }
}

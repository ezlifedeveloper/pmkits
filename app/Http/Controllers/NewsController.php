<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function show($id)
    {
        // $page = $request->page ?? 1;
        $data = News::where('slug', $id)->first();
        $update = News::where('slug', $id)->increment('read');

        return view('web.news-detail')
            ->with('data', $data)
            ->with('title', $data->title)
            ->with('menu', 'News');
    }
}

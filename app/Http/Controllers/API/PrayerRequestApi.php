<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\PrayerRequest;
use Illuminate\Http\Request;

/**
 * @group PrayerRequest Management
 *
 * APIs for managing PrayerRequest Data
 * @authenticated
 */

class PrayerRequestApi extends Controller
{

    /**
     * Get PrayerRequest
     *
     * Get PrayerRequest Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string PrayerRequest name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data PrayerRequest Data sorted by distance.
     */

    public function index(Request $request)
    {
        // $prayerRequest = PrayerRequest::all();
        // return response()->json(['massage'=>'Success see prayer request', 'data'=>$prayerRequest]);
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = PrayerRequest::when($search, function ($query) use ($search) {
            return $query->where('user', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('user', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get PrayerRequest Data', 'data' => $data], 200);
    }

    /**
     * Post Prayer Request
     *
     * Post Prayer Request, on success you'll get a 200 OK response.
     *
     * @bodyParam user int User's id who created the Prayer Request  Example: 1
     * @bodyParam prayer string required Things to pray for  Example: Sidang  TA lancar dan lulus
     * @bodyParam report string The content of the prayer of the person who prays  Example: Ya bapa kami semoga TA teman kami berjalan lancar
     * @bodyParam visible boolean required Visible or not  Example: 0
     * @bodyParam showed boolean required Showed or not  Example: 0
     * @bodyParam prayed boolean required Has been prayed or not  Example: 0
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Recently entered Prayer Request data
     */

    public function store(Request $request)
    {
        $prayerRequest = PrayerRequest::create([
            'user' => $request->user,
            'prayer' => $request->prayer,
            'report' => $request->report,
            'visible' => $request->visible,
            'showed' => $request->showed,
            'prayed' => $request->prayed,
        ]);
        return response()->json(['success' => true, 'result' => 'Successfully Store Prayer Request', 'data' => $prayerRequest]);
    }
}

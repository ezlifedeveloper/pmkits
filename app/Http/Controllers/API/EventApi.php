<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;

/**
 * @group Event Management
 *
 * APIs for managing Event Data
 * @authenticated
 */
class EventApi extends Controller
{
    /**
     * Get Event List
     *
     * Get Event List, on success you'll get a 200 OK response.
     *
     * @queryParam search string Event name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Event Data sorted by start.
     */

    public function index(Request $request)
    {
        // $event = Event::all();
        // return response()->json(['massage' => 'Success Event', 'data' => $event]);
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = Event::when($search, function ($query) use ($search) {
            return $query->where('name', 'like', '%' . $search . '%')
                ->orWhere('start', 'like', '%' . $search . '%')
                ->orWhere('end', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('start', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Event Data', 'data' => $data], 200);
    }

    /**
     * Get Event By ID
     *
     * Get Event By ID, on success you'll get a 200 OK response.
     *
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Event Data obtained by id.
     */

    public function show($id)
    {
        $event = Event::find($id);
        return response()->json(['success' => true, 'result' => 'Successfully Get Event Data', 'data' => $event], 200);
    }
}

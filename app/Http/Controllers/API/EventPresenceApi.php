<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\EventPresence;
use Illuminate\Http\Request;

/**
 * @group Event Presence
 *
 * APIs for managing Event Data
 * @authenticated
 */
class EventPresenceApi extends Controller
{

    /**
     * Post Event Presence
     *
     * Post Event Presence, on success you'll get a 200 OK response.
     *
     * @bodyParam event int Event id Example: 1
     * @bodyParam user int required User's id who attended the event  Example: 1
     * @bodyParam name string required User's name who attended the event  Example: Herri
     * @bodyParam from string required User's department who attended the event  Example: Informatika
     * @bodyParam created_by int User's id who attended the event  Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Recently entered Event Precence data
     */
    public function store(Request $request)
    {
        $eventPresence = EventPresence::create([
            'event' => $request->event,
            'user' => $request->user,
            'name' => $request->name,
            'from' => $request->from,
            'created_by' => $request->created_by,
        ]);
        return response()->json(['success' => true, 'result' => 'Successfully Store Event Precense', 'data' => $eventPresence]);
    }
}

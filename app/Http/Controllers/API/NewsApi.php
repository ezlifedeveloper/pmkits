<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

/**
 * @group News Management
 *
 * APIs for managing News Data
 * @authenticated
 */

class NewsApi extends Controller
{

    /**
     * Get News
     *
     * Get News Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string News name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data News Data sorted by distance.
     */
    public function index(Request $request)
    {
        // $news = News::all();

        // return response()->json(['massage' => 'Success News', 'data' => $news]);

        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = News::when($search, function ($query) use ($search) {
            return $query->where('title', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('title', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get News Data', 'data' => $data], 200);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Renungan;
use Illuminate\Http\Request;

/**
 * @group Renungan Management
 *
 * APIs for managing Renungan Data
 * @authenticated
 */
class RenunganApi extends Controller
{

    /**
     * Get Renungan
     *
     * Get Renungan Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string Renungan name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Renungan Data sorted by distance.
     */

    public function index(Request $request)
    {
        // $renungan = Renungan::all();
        // return response()->json(['massage' => 'Success Renungan', 'data' => $renungan]);
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = Renungan::when($search, function ($query) use ($search) {
            return $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('verse', 'like', '%' . $search . '%')
                ->orWhere('date', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('date', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Renungan Data', 'data' => $data], 200);
    }
}

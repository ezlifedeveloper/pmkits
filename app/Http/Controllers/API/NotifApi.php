<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Notif;
use Illuminate\Http\Request;

/**
 * @group Notif Management
 *
 * APIs for managing Notif Data
 * @authenticated
 */

class NotifApi extends Controller
{
    /**
     * Get Notif
     *
     * Get Notif Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string Notif name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Notif Data sorted by distance.
     */

    public function show($id)
    {
        $data = Notif::find($id);
        return response()->json(['success' => true, 'result' => 'Successfully Get Notif Data', 'data' => $data], 200);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * @group Auth Management
 *
 * APIs for User Auth
 */
class AuthApi extends Controller
{
    /**
     * Login Default
     *
     * Login Default, on success you'll get a 200 OK response.
     * Return 401 when user is not found / authenticated.
     * Return 422 when parameter is not filled.
     *
     * @bodyParam username string required User's username Example: prasetyon
     * @bodyParam password string required User's password. Example: passw0rd
     * @bodyParam token string required User's device token. Example: aBc4e6Gh
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data User list.
     */
    public function login(Request $request)
    {
        if (!$request->filled('username') || !$request->filled('password') || !$request->filled('token')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $username = $request->username;
        $pwd = $request->password;

        $data = User::where('username', $username)->first();

        if ($data && /*$data->email_verified_at &&*/ Hash::check($pwd, $data->password)) {
            $data->api_token = $this->generateToken($data->id, $data->username, $data->email);
            $this->saveExpoToken($data->id, $request->token);
            Helper::recordUserLog($data->id, 'Mobile Default Login ' . $request->token);

            return response()->json(['success' => true, 'result' => 'Successfully Logged In', 'data' => $data], 200);
        } else if (!$data) {
            return response()->json(['success' => false, 'result' => 'ID not registered'], 401);
        } /*else if (!$data->email_verified_at) {
            return response()->json(['success' => false, 'result' => 'Account not verified, please activate your account'], 401);
        }*/ else if (!Hash::check($pwd, $data->password)) {
            return response()->json(['success' => false, 'result' => 'Password and id combination not match'], 401);
        } else {
            return response()->json(['success' => false, 'result' => 'Oops! Something went wrong'], 500);
        }
    }

    /**
     * Logout
     *
     * Logout, on success you'll get a 200 OK response.
     * Return 401 when user is not found / authenticated.
     * Return 422 when parameter is not filled.
     *
     * @authenticated
     * @bodyParam id string required User's id Example: 999
     * @bodyParam token string required User's device token. Example: aBc4e6Gh
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data User list.
     */
    public function logout(Request $request)
    {
        $auth = User::where('id', $request->id)->first();
        $this->deleteExpoToken($auth->id, $request->token);

        return response()->json(['success' => true, 'result' => 'Successfully Logged Out', 'data' => $auth], 200);
    }

    private function generateToken($id, $username, $email)
    {
        $token = hash('sha256', $username . $email);
        $user = User::where('id', $id)->update(['api_token' => $token]);
        return $token;
    }

    private function saveExpoToken($id, $token)
    {
        $user = User::where('id', $id)->first();
        $arr = explode(',', $user->expo_token);
        if (($key = array_search($token, $arr)) === false) {
            $arr[] = $token;
        }

        $user = User::where('id', $id)->update(['expo_token' => implode(',', $arr)]);
    }

    private function deleteExpoToken($id, $token)
    {
        $user = User::where('id', $id)->first();
        $arr = explode(',', $user->expo_token);

        if (($key = array_search($token, $arr)) !== false) {
            unset($arr[$key]);
        }
        $user = User::where('id', $id)->update(['expo_token' => implode(',', $arr)]);
    }
}

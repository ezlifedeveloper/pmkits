<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * @group User Management
 *
 * APIs for managing User Data
 * @authenticated
 */

class UserApi extends Controller
{

    /**
     * Get User List
     *
     * Get User List, on success you'll get a 200 OK response.
     *
     * @queryParam search string User name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data User Data sorted by distance.
     */

    public function index(Request $request)
    {
        // $user = User::all();
        // return response()->json(['massage' => 'Success', 'data'=> $user ]);
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = User::when($search, function ($query) use ($search) {
            return $query->where('name', 'like', '%' . $search . '%')
                ->orWhere('username', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('username', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get User Data', 'data' => $data], 200);
    }

    /**
     * Get User Data
     *
     * Get User Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string User name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data User Data sorted by distance.
     */
    public function show($id)
    {
        $data = User::find($id);
        return response()->json(['success' => true, 'result' => 'Successfully Get User Data', 'data' => $data], 200);
    }

    /**
     * Put User
     *
     * Put User, on success you'll get a 200 OK response.
     *
     * @bodyParam username string required Unique username of the user who want to created  Example: Herri
     * @bodyParam name string required Full name of the user who want to created  Example: Herri Purba
     * @bodyParam password string required Password of the user who want to created  Example: Herri
     * @bodyParam role string required Role of the user who want to created  Example: 2
     * @bodyParam access string Access of the user who want to created  Example: Dope
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Recently entered User data
     */

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password,
            'role' => $request->role,
            'access' => $request->access,
        ]);
        return response()->json(['success' => true, 'result' => 'Successfully Update User', 'data' => $user]);
    }

    /**
     * Post User
     *
     * Post User, on success you'll get a 200 OK response.
     *
     * @bodyParam username string required Unique username of the user who want to created  Example: Herri
     * @bodyParam name string required Full name of the user who want to created  Example: Herri Purba
     * @bodyParam password string required Password of the user who want to created  Example: Herri
     * @bodyParam role string required Role of the user who want to created  Example: 2
     * @bodyParam access string Access of the user who want to created  Example: Dope
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Recently entered User data
     */

    public function store(Request $request)
    {
        if (!$request->filled('name') || !$request->filled('username') || !$request->filled('password')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $usernameCheck = User::where("username", $request->username)->first();
        if ($usernameCheck) return response()->json(['success' => false, 'result' => 'Username already registered']);

        $data = new User();
        $data->username = $request->username;
        $data->name = $request->name;
        $data->password = bcrypt($request->password);
        $data->role = $request->role;
        $data->access = $request->access;
        $data->save();

        return response()->json(['success' => true, 'result' => 'User Registered', 'data' => $data]);
    }
}

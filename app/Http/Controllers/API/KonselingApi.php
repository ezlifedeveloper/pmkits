<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Counseling;
use Illuminate\Http\Request;

/**
 * @group Konseling Management
 *
 * APIs for managing Konseling Data
 * @authenticated
 */
class KonselingApi extends Controller
{

    /**
     * Get Konseling
     *
     * Get Konseling Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string Konseling name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Konseling Data sorted by distance.
     */

    public function index(Request $request)
    {
        // $koseling = Counseling::all();
        // return response()->json(['massage'=>'Success see koseling', 'data'=>$koseling]);
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = Counseling::when($search, function ($query) use ($search) {
            return $query->where('user', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('user', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Counseling Data', 'data' => $data], 200);
    }

     /**
     * Post Konseling
     *
     * Post Konseling, on success you'll get a 200 OK response.
     *
     * @bodyParam user int required User's id who created the Counseling  Example: 1
     * @bodyParam status int required Counseling has been carried out or not  Example: 0
     * @bodyParam type int required Type of Counseling  Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Recently entered Konseling data
     */

    public function store(Request $request)
    {
        // $koseling = Counseling::create($request->all());
        $koseling = Counseling::create([
            'user' => $request->user,
            'status' => $request->status,
            'type' => $request->type

        ]);
        return response()->json(['success' => true, 'result' => 'Successfully Store Konseling', 'data' => $koseling]);
    }
}

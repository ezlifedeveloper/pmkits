<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Kepengurusan;
use Illuminate\Http\Request;

/**
 * @group Kepengurusan Management
 *
 * APIs for managing Kepengurusan Data
 * @authenticated
 */
class KepengurusanManagementApi extends Controller
{
    /**
     * Get Kepengurusan
     *
     * Get Kepengurusan Data, on success you'll get a 200 OK response.
     *
     * @queryParam search string Kepengurusan name to be searched.
     * @queryParam limit int Number of data shown. Example: 3
     * @queryParam page int Selected page to be shown. Example: 1
     *
     * @responseField success The status of this API request.
     * @responseField result Description of this API request.
     * @responseField data Kepengurusan Data sorted by distance.
     */
    public function index(Request $request)
    {
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? null;

        $data = Kepengurusan::when($search, function ($query) use ($search) {
            return $query->where('name', 'like', '%' . $search . '%')
                ->orWhere('start', 'like', '%' . $search . '%')
                ->orWhere('end', 'like', '%' . $search . '%');
        })->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        })->when($page, function ($query) use ($page) {
            return $query->offset($page);
        })->orderBy('start', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get Kepengurusan Data', 'data' => $data], 200);
    }
}

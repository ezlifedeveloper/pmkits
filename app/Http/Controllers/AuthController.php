<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        if (!Auth::check()) return view('login');
        else return redirect()->route('home');
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $pwd = $request->password;
        $year = $request->year;

        if (Auth::attempt(['username' => $username, 'password' => $pwd])) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->back()->with('alert-danger', 'Terjadi kesalahan dalam login. Cek kembali Username dan Password Anda');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Renungan;
use Illuminate\Http\Request;

class RenunganController extends Controller
{
    public function show($id)
    {
        // $page = $request->page ?? 1;
        $data = Renungan::where('slug', $id)->first();
        $update = Renungan::where('slug', $id)->increment('read');

        return view('web.renungan-detail')
            ->with('data', $data)
            ->with('title', $data->title)
            ->with('menu', 'Renungan');
    }
}

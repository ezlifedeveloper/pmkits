<?php

namespace App\Helpers;

use App\Models\Kepengurusan;
use App\Models\Mapping;
use App\Models\UserLog;

class Helper
{
    public static function recordUserLog($id, $status, $lat = null, $lng = null)
    {
        UserLog::create([
            'user_id' => $id,
            'status' => $status,
            'lat' => $lat,
            'lng' => $lng
        ]);
    }

    public static function getIdKepengurusanactive()
    {
        $active = Kepengurusan::where('status', true)->first();
        return $active->id;
    }

    public static function uploadFile($file, $fixedFileName, $folder)
    {
        $size = $file->getSize();
        $ext = '.' . $file->getClientOriginalExtension();

        $destinationPath = public_path() . $folder;
        $uploadedname = $fixedFileName . $ext;

        $file->move($destinationPath, $uploadedname);
        $destinationPath = env('APP_URL') . $folder;

        return $destinationPath . $uploadedname;
    }

    public static function cleanStr($string)
    {
        // Replaces all spaces with hyphens.
        $string = str_replace(' ', '-', $string);

        // Removes special chars.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        // Replaces multiple hyphens with single one.
        $string = preg_replace('/-+/', '-', $string);

        return $string;
    }
}

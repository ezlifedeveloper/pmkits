<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrayerRequest extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function submitter()
    {
        return $this->belongsTo(User::class, 'user');
    }
}

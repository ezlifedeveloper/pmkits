<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatOrganisasi extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $dates = ['start', 'finish'];
    protected $with = ['parent', 'creator'];

    public function parent()
    {
        return $this->belongsTo(Kepengurusan::class, 'kepengurusan');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user');
    }
}

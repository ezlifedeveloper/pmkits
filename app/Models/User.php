<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $with = ['roleUser'];
    protected $guarded = [];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function roleUser()
    {
        return $this->belongsTo(UserType::class, 'role');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function pendidikan()
    {
        return $this->hasMany(RiwayatPendidikan::class, 'user')->orderBy('start', 'desc');
    }

    public function organisasi()
    {
        return $this->hasMany(RiwayatOrganisasi::class, 'user')->orderBy('start', 'desc');;
    }
}

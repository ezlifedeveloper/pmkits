<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPresence extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['participant'];

    public function participant()
    {
        return $this->belongsTo(User::class, 'user');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $dates = ['start', 'end', 'open', 'close'];

    public function file()
    {
        return $this->hasMany(EventImage::class, 'event');
    }

    public function presence()
    {
        return $this->hasMany(EventPresence::class, 'event');
    }
}
